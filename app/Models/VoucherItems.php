<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VoucherItems extends Model 
{

    protected $table = 'voucher_items';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('voucher_id', 'category_id', 'amount', 'total', 'price');

    public function voucher()
    {
        return $this->hasOne('App\Models\Voucher');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

}