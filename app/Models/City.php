<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model 
{

    protected $table = 'cities';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('name_ar', 'name_en','area_id');
    protected $hidden = array('name_ar', 'name_en');

    public function neighborhoods()
    {
        return $this->hasMany('App\Models\Neighborhood');
    }
    public function area()
    {
        return $this->belongsTo('App\Models\Area');
    }

    public function borders(){

        return $this->morphMany('App\Models\Border', 'model');
    }

}
