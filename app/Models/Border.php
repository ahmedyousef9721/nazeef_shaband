<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Border extends Model 
{

    protected $table = 'borders';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('model_id', 'model_type', 'lat', 'lng');


    public function model(){
        return $this->morphTo();
    }
}
