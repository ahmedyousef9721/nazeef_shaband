<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable=['ticket_number','title', 'ticked_number', 'model_type', 'model_id', 'priority', 'status', 'has_new_message'];


    public function client(){

        return $this->morphTo('model');
    }

    public function messages()
    {

        return $this->hasMany('App\Models\TicketMessage','ticket_id');

    }
}
