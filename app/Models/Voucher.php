<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Voucher extends Model 
{

    protected $table = 'vouchers';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('client_id', 'laundry_id', 'delivery_from_id', 'delivery_to_id', 'is_fast', 'status', 'delivery_from_cost', 'delivery_to_cost', 'delivery_from_received_at', 'delivery_to_received_at', 'laundry_reciver_at', 'finished_at', 'client_received_at', 'code');

    public function refused()
    {
        return $this->hasMany('App\Models\Voucher');
    }

}