<?php

namespace App;

use App\Traits\CreateApiToken;
use App\Traits\HashPassword;
use App\Traits\HasPhoneCode;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Delivery extends Authenticatable
{


    protected $table = 'deliveries';
    public $timestamps = true;

    use SoftDeletes,HashPassword,HasPhoneCode,CreateApiToken;

    protected $dates = ['deleted_at'];
    protected $fillable = array('name', 'email', 'phone', 'password', 'lat', 'lng', 'image', 'car_type', 'type', 'user_id', 'salary', 'percent', 'verify_mobile', 'mobile_verified','active', 'personal_id', 'drive_license', 'car_license', 'criminal_testimony_pic', 'car_front', 'car_back','neighborhood_id');

    protected  $hidden=['password'];
    public function vouchers_form()
    {
        return $this->hasMany('App\Models\Voucher', 'delivery_from_id');
    }

    public function voucher_to()
    {
        return $this->morphMany('Setting', 'voucher_to_id');
    } public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function tokens()
    {
        return $this->morphMany('App\Models\Token','model');
    }
    public function neighborhood(){

        return $this->belongsTo('App\Models\Neighborhood','neighborhood_id');
    }

}
