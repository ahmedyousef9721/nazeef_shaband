<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\CreateApiToken;
use App\Traits\HashPassword;
use App\Traits\HasPhoneCode;

class Client extends Authenticatable
{
    use SoftDeletes, CreateApiToken, HashPassword, HasPhoneCode;

    protected $table = 'clients';
    public $timestamps = true;


    protected $dates = ['deleted_at'];
    protected $fillable = array('name', 'email', 'password', 'image', 'phone', 'verify_mobile', 'mobile_verified', 'balance', 'active', 'fb_token', 'twitter_token', 'google_token', 'is_boot', 'api_token');

    protected $hidden=['password'];
    public function vouchers()
    {
        return $this->hasOne('App\Models\Voucher');
    }

    public function tokens()
    {
        return $this->morphMany('App\Models\Token', 'model');
    }

    public function addresses()
    {
        return $this->hasMany('App\Models\Address', 'client_id');
    }

}
