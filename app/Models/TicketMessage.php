<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketMessage extends Model
{
    protected $fillable = ['ticket_id','replier_type', 'replier_id', 'message', 'file'];

    public function replier()
    {
        return $this->morphTo('replier');

    }
    public function ticket()
    {
        return $this->belongsTo('App\Model\Ticket','ticket_id');

    }

}


