<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Neighborhood extends Model 
{

    protected $table = 'neighborhoods';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('name_ar', 'name_en', 'city_id');
    protected $hidden = array('name_ar', 'name_en', 'city_id');

    public function borders()
    {
        return $this->morphMany('App\Models\Border', 'model');
    }
    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

}
