<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    protected $fillable=['model_id','model_type','token','device-type'];


    public function  user(){

        return $this->morphTo('model');
    }
}
