<?php

namespace App;

use App\Traits\CreateApiToken;
use App\Traits\HashPassword;
use App\Traits\HasPhoneCode;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Laundry extends Authenticatable
{

    protected $table = 'laundries';
    public $timestamps = true;

    use SoftDeletes,HasPhoneCode,HashPassword,CreateApiToken;

    protected $dates = ['deleted_at'];
    protected $fillable = array( 'name', 'email', 'password', 'image', 'phone', 'Commercial_number', 'address', 'clothes', 'carpets',
        'mattresses', 'lat', 'lng', 'neighborhood_id', 'created_at', 'updated_at', 'deleted_at', 'clean', 'dry_clean',
        'clean_ironing', 'api_token', 'active', 'verify_mobile', 'mobile_verified', 'Commercial_cert', 'laundry_personal_id',
        'shop_personal_id');

    public function neighborhood()
    {
        return $this->belongsTo('App\Models\Neighborhood');
    }

    public function vouchers()
    {
        return $this->hasMany('App\Models\Voucher');
    }
    public function tokens()
    {
        return $this->morphMany('App\Models\Token','model');
    }

}
