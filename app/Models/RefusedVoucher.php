<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RefusedVoucher extends Model 
{

    protected $table = 'refused_vouchers';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('voucher_id', 'model_id', 'model_type', 'note');

    public function voucher()
    {
        return $this->hasOne('App\Models\Voucher');
    }

    public function model()
    {
        return $this->morphTo();
    }

}