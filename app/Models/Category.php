<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model 
{

    protected $table = 'categories';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array( 'name_ar', 'name_en', 'image', 'type', 'clean_price', 'dry_clean_price', 'clean_ironing_price', 'fast_clean_price', 'fast_dry_clean_price', 'fast_clean_ironing_price');
    protected $hidden = array('name_ar');

}
