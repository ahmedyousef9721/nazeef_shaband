<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Area extends Model 
{

    protected $table = 'areas';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('name_ar', 'name_en');
    protected $hidden = array('name_ar', 'name_en');

    public function Cities()
    {
        return $this->hasMany('App\Models\City');
    }
     public function neighborhoods()
    {
        return $this->hasManyThrough('App\Models\Neighborhood','App\Models\City','city_id','area_id');
    }
    public function borders()
    {
        return $this->morphMany('App\Models\Border', 'model');
    }

}
