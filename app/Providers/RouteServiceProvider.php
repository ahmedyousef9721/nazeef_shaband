<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';
    protected $laundry_api_namespace = 'App\Http\Controllers\api\laundries';
    protected $client_api_namespace = 'App\Http\Controllers\api\clients';
    protected $delivery_api_namespace = 'App\Http\Controllers\api\deliveries';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapAdminRoutes();
        $this->mapClientApiRoutes();
        $this->mapDeliveryApiRoutes();
        $this->mapLaundryApiRoutes();
        //
    }

    /**
     * Define the "admin" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapAdminRoutes()
    {
        Route::prefix('admin')
            ->middleware(['web'])
            ->namespace($this->namespace)
            ->group(base_path('routes/admin.php'));
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }

    protected function mapClientApiRoutes()
    {
        Route::prefix('api/client')
            ->middleware(['api', 'apiLang'])
            ->namespace($this->client_api_namespace)
            ->group(base_path('routes/client_api.php'));
    }

    protected function mapLaundryApiRoutes()
    {
        Route::prefix('api/laundry')
            ->middleware(['api', 'apiLang'])
            ->namespace($this->laundry_api_namespace)
            ->group(base_path('routes/laundry_api.php'));
    }

    protected function mapDeliveryApiRoutes()
    {
        Route::prefix('api/delivery')
            ->middleware(['api', 'apiLang'])
            ->namespace($this->delivery_api_namespace)
            ->group(base_path('routes/delivery_api.php'));
    }
}
