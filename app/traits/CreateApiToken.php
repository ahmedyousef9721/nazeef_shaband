<?php
namespace  App\Traits;

trait  CreateApiToken {
    /**
     * Boot the has password trait for a model.
     *
     * @return void
     */
    public static function bootCreateApiToken()
    {

        static::creating(function ($user) {
            $user->api_token = str_random(60);
        });



    }
}
