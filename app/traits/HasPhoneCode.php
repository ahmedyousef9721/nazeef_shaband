<?php
namespace  App\Traits;


trait  HasPhoneCode{
    /**
     * Boot the has password trait for a model.
     *
     * @return void
     */
    public static function bootHasPhoneCode()
    {

        static::creating(function ($user) {
            $user->verify_mobile =  rand(00000,99999);
        });



    }
}
