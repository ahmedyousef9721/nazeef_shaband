<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\App;

class ApiLang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if($request->hasHeader('Accept-Language')){

            $language=$request->hasHeader('Accept-Language');

            App::setLocale($language);

            Carbon::setLocale($request->header('Accept-Language'));

            return $next($request);
        }
        return $next($request);
    }
}
