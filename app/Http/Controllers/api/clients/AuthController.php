<?php

namespace App\Http\Controllers\api\clients;

use App\Client;
use App\Models\Token;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{

    public function Register(Request $request)
    {

        $rules = [
            'name' => 'required|string',
            'email' => 'required|email|unique:clients,email',
            'password' => 'required|confirmed',
            'phone' => 'required|numeric|unique:clients,phone',
            'policy'=>'required'
           // 'token' => 'required|string'
        ];


        $validation = validator()->make($request->all(), $rules);

        if ($validation->fails()) {

            return responseJson('0', $validation->errors()->first());

        }
        $requests = $request->except('image');

        DB::beginTransaction();
        $client = Client::create($requests)->load('addresses');
     /*   $token = $client->tokens()->create([
            'token' => $request->token,
            'device' => $request->headers->get('device-type'),
        ]);*/

        //TODO :: add message phone verify

        DB::commit();
        if ($client) {
            $client=Client::with('addresses')->find($client->id);
            return responseJson('1', trans('api.signup.success'), ['client' => $client]);
        } else {
            return responseJson('0', trans('api.signup.error'));

        }
    }

    //--------------------------------------------- Login
    public function Login(Request $request)
    {
        $rules = [
            'email' => 'required',
            'password' => 'required|string',
          //  'token' => 'required|string'
        ];


        $validation = validator()->make($request->all(), $rules);

        if ($validation->fails()) {
            return responseJson('0', $validation->errors()->first());
        }

        $client = Client::with('addresses')->where('email', $request->input('email'))->orWhere('phone', $request->input('email'))->first();



        if (!is_null($client)&&Hash::check($request->password, $client->password)) {


         /*   $token = $client->tokens()->create([
                'token' => $request->token,
                'device' => $request->headers->get('device-type'),
            ]);*/
            return responseJson('1', trans('api.login.success'), ['client' => $client]);


        } else {

            return responseJson('0', trans('api.login.notCorrect'));

        }
    }


    //--------------------------------------------- Show & Edit Student profile
    public function Profile(Request $request)
    {
        $client = Auth::user();

        $rules = [
            'name' => 'nullable',
            'email' => 'nullable|email|unique:clients,email,' . $client->id,
            'password' => 'nullable|confirmed',
            'phone' => 'nullable|numeric|unique:clients,phone,' . $client->id,
            'image' => 'nullable|image',
            'old_password' => 'required_if:password,!=,null',

        ];
        $validation = validator()->make($request->all(), $rules);

        if ($validation->fails()) {

            return responseJson('0', $validation->errors()->first());

        }

        if ($request->has('name') && $request->name != NULL) {
            $client->update($request->only('name'));
        }


        if ($request->has('password') && $request->password != NULL) {


            if (Hash::check($request->old_password, Auth::user()->password)) {
                $client->update(['password' => $request->password]);
            } else {
                return responseJson(0, trans('api.update.password_error'), null);
            }

        }

        if ($request->has('phone') && $request->phone != NULL) {
            $client->update($request->only('phone'));
        }


        if ($request->has('email') && $request->email != NULL) {

            $client->update($request->only('email'));
        }

        

        if ($request->hasFile('image') && $request->image != NULL) {
            $client->update(['image' => saveImage($request->image, '/clients')]);

        }

        $client = Auth::user()->load('addresses');
        return responseJson(1, trans('api.update.success'), ['client' => $client]);

    }




    public function verifyProfile(Request $request)
    {

        $rules =
            [
            'email' => 'required|email|exists:clients,email',
            'verify_mobile' => 'required|exists:clients,verify_mobile',
            ];
        $validation = validator()->make($request->all(), $rules);
        if ($validation->fails()) {
            return responseJson('0', $validation->errors()->first());
        }
        $client = Client::with('addresses')->where('email', $request->email)->where($request->only('verify_mobile'));
        if ($client == null) {
            return responseJson(1, trans('api.update.error'));
        }
        $client->update(['mobile_verified' => Carbon::now()]);
        $client = Client::with('addresses')->where('email', $request->email)->where($request->only('verify_mobile'))->first();
        return responseJson(1, trans('api.update.success'), ['client' => $client]);
    }

    public function resend_verify(Request $request)
    {

        $rules = [
            'email' => 'required|email|exists:clients,email',
        ];
        $validation = validator()->make($request->all(), $rules);
        if ($validation->fails()) {
            return responseJson('0', $validation->errors()->first());
        }
        $client = Client::with('addresses')->where('email', $request->email)->first();
        if ($client == null) {
            return responseJson(1, trans('api.user.resend_code_error'));
        }
        //TODO :: add message phone verify

        return responseJson(0, trans('api.user.resend_code'), $client);
    }

    public function logout(Request $request)
    {
        $rules = [
            'token' => 'required|string|exists:tokens,token,model_id,' . Auth::id()
        ];
        $validation = validator()->make($request->all(), $rules);

        if ($validation->fails()) {
            return responseJson('0', $validation->errors()->first());
        }
        $token = Token::where('token', $request->token)->where('user_id', Auth::id())->delete();

        return responseJson(1, trans('api.logout'));
    }

}
