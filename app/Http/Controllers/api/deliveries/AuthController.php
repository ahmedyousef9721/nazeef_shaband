<?php

namespace App\Http\Controllers\api\deliveries;

use App\Delivery;
use App\Models\Token;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function Register(Request $request)
    {

        $rules = [
            'name' => 'required|string',
            'email' => 'required|email|unique:deliveries,email',
            'password' => 'required|confirmed',
            'phone' => 'required|numeric|unique:deliveries,phone',
            'policy' => 'required',
            // 'token' => 'required|string'
        ];


        $validation = validator()->make($request->all(), $rules);

        if ($validation->fails()) {

            return responseJson('0', $validation->errors()->first());

        }
        $requests = $request->except('image');

        DB::beginTransaction();
        $delivery = Delivery::create($requests);
        /*   $token = $delivery->tokens()->create([
               'token' => $request->token,
               'device' => $request->headers->get('device-type'),
           ]);*/

        //TODO :: add message phone verify

        DB::commit();
        if ($delivery) {
            $delivery=Delivery::find($delivery->id);
            return responseJson('1', trans('api.signup.success'), ['delivery' => $delivery]);
        } else {
            return responseJson('0', trans('api.signup.error'));

        }
    }

    //--------------------------------------------- Login
    public function Login(Request $request)
    {
        $rules = [
            'email' => 'required',
            'password' => 'required|string',
            //  'token' => 'required|string'
        ];


        $validation = validator()->make($request->all(), $rules);

        if ($validation->fails()) {
            return responseJson('0', $validation->errors()->first());
        }

        $delivery = Delivery::where('email', $request->input('email'))->orWhere('phone', $request->input('email'))->first();



        if (!is_null($delivery)&&Hash::check($request->password, $delivery->password)) {

            if($delivery->active){

            return responseJson('1', trans('api.login.success'), ['delivery' => $delivery]);
            }
            else{

                return responseJson('0', trans('api.login.account_not_active'));
            }


        } else {

            return responseJson('0', trans('api.login.notCorrect'));

        }
    }


    //--------------------------------------------- Show & Edit Student profile
    public function Profile(Request $request)
    {
        $delivery = Auth::user();

        $rules = [
            'name' => 'nullable',
            'email' => 'nullable|email|unique:deliveries,email,' . $delivery->id,
            'password' => 'nullable|confirmed',
            'phone' => 'nullable|numeric|unique:deliveries,phone,' . $delivery->id,
            'image' => 'nullable|image',
            'old_password' => 'required_if:password,!=,null',
            'drive_license'=>'nullable|image',
            'car_license'=>'nullable|image',
            'criminal_testimony_pic'=>'nullable|image',
            'car_front'=>'nullable|image',
            'car_back'=>'nullable|image'



        ];
        $validation = validator()->make($request->all(), $rules);

        if ($validation->fails()) {

            return responseJson('0', $validation->errors()->first());

        }

        if ($request->has('name') && $request->name != NULL) {
            $delivery->update($request->only('name'));
        }


        if ($request->has('password') && $request->password != NULL) {


            if (Hash::check($request->old_password, Auth::user()->password)) {
                $delivery->update(['password' => $request->password]);
            } else {
                return responseJson(0, trans('api.update.password_error'), null);
            }

        }

        if ($request->has('phone') && $request->phone != NULL) {
            $delivery->update($request->only('phone'));
        }


        if ($request->has('email') && $request->email != NULL) {

            $delivery->update($request->only('email'));
        }



        if ($request->hasFile('image') && $request->image != NULL) {
            $delivery->update(['image' => saveImage($request->image, '/deliveries')]);

        }
        if ($request->hasFile('car_license') && $request->car_license != NULL) {
            $delivery->update(['car_license' => saveImage($request->car_license, '/deliveries')]);

        }
        if ($request->hasFile('criminal_testimony_pic') && $request->criminal_testimony_pic != NULL) {
            $delivery->update(['criminal_testimony_pic' => saveImage($request->criminal_testimony_pic, '/deliveries')]);
        }
        if ($request->hasFile('car_front') && $request->car_front != NULL) {
            $delivery->update(['car_front' => saveImage($request->car_front, '/deliveries')]);
        }


        if ($request->hasFile('car_back') && $request->car_back != NULL) {
            $delivery->update(['car_back' => saveImage($request->car_back, '/deliveries')]);
        }

        if ($request->hasFile('drive_license') && $request->drive_license != NULL) {
            $delivery->update(['drive_license' => saveImage($request->drive_license, '/deliveries')]);
        }


        $delivery = Auth::user();
        return responseJson(1, trans('api.update.success'), ['delivery' => $delivery]);
    }




    public function verifyProfile(Request $request)
    {

        $rules =
            [
                'email' => 'required|email|exists:deliveries,email',
                'verify_mobile' => 'required|exists:deliveries,verify_mobile',
            ];
        $validation = validator()->make($request->all(), $rules);
        if ($validation->fails()) {
            return responseJson('0', $validation->errors()->first());
        }
        $delivery = Delivery::with('addresses')->where('email', $request->email)->where($request->only('verify_mobile'));
        if ($delivery == null) {
            return responseJson(1, trans('api.update.error'));
        }
        $delivery->update(['mobile_verified' => Carbon::now()]);
        $delivery = Delivery::where('email', $request->email)->where($request->only('verify_mobile'))->first();
        return responseJson(1, trans('api.update.success'), ['delivery' => $delivery]);
    }

    public function resend_verify(Request $request)
    {

        $rules = [
            'email' => 'required|email|exists:deliveries,email',
        ];
        $validation = validator()->make($request->all(), $rules);
        if ($validation->fails()) {
            return responseJson('0', $validation->errors()->first());
        }
        $delivery = Delivery::where('email', $request->email)->first();
        if ($delivery == null) {
            return responseJson(1, trans('api.user.resend_code_error'));
        }
        //TODO :: add message phone verify

        return responseJson(0, trans('api.user.resend_code'), $delivery);
    }

    public function logout(Request $request)
    {
        $rules = [
            'token' => 'required|string|exists:tokens,token,model_id,' . Auth::id()
        ];
        $validation = validator()->make($request->all(), $rules);

        if ($validation->fails()) {
            return responseJson('0', $validation->errors()->first());
        }
        $token = Token::where('token', $request->token)->where('user_id', Auth::id())->delete();

        return responseJson(1, trans('api.logout'));
    }

}
