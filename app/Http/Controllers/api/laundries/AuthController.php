<?php

namespace App\Http\Controllers\api\laundries;

use App\Laundry;
use App\Models\Token;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function Register(Request $request)
    {

        $rules = [
            'name' => 'required|string',
            'email' => 'required|email|unique:laundries,email',
            'password' => 'required|confirmed',
            'phone' => 'required|numeric|unique:laundries,phone',
            'policy' => 'required',
            'Commercial_number' => 'required|string',
            'address' => 'required|string',

        ];

        /*

        */
        $validation = validator()->make($request->all(), $rules);

        if ($validation->fails()) {

            return responseJson('0', $validation->errors()->first());

        }
        $requests = $request->except('image');

        DB::beginTransaction();
        $laundry = Laundry::create($requests);
        /*   $token = $laundry->tokens()->create([
               'token' => $request->token,
               'device' => $request->headers->get('device-type'),
           ]);*/

        //TODO :: add message phone verify

        DB::commit();
        if ($laundry) {
            $laundry = Laundry::find($laundry->id);
            return responseJson('1', trans('api.signup.success'), ['laundry' => $laundry]);
        } else {
            return responseJson('0', trans('api.signup.error'));

        }
    }

    //--------------------------------------------- Login
    public function Login(Request $request)
    {
        $rules = [
            'email' => 'required',
            'password' => 'required|string',
            //  'token' => 'required|string'
        ];


        $validation = validator()->make($request->all(), $rules);

        if ($validation->fails()) {
            return responseJson('0', $validation->errors()->first());
        }

        $laundry = Laundry::where('email', $request->input('email'))->orWhere('phone', $request->input('email'))->first();


        if (!is_null($laundry) && Hash::check($request->password, $laundry->password)) {

            if ($laundry->active) {

                return responseJson('1', trans('api.login.success'), ['laundry' => $laundry]);
            } else {

                return responseJson('0', trans('api.login.account_not_active'));
            }


        } else {

            return responseJson('0', trans('api.login.notCorrect'));

        }
    }


    //--------------------------------------------- Show & Edit Student profile
    public function Profile(Request $request)
    {
        $laundry = Auth::user();

        $rules = [
            'name' => 'nullable',
            'email' => 'nullable|email|unique:laundries,email,' . $laundry->id,
            'password' => 'nullable|confirmed',
            'phone' => 'nullable|numeric|unique:laundries,phone,' . $laundry->id,
            'image' => 'nullable|image',
            'old_password' => 'required_if:password,!=,null',
            'Commercial_number' => 'nullable|unique:laundries,Commercial_number,' . $laundry->id,
            'address' => 'nullable',
            'clothes' => 'nullable',
            'carpets' => 'nullable',
            'mattresses' => 'nullable',
            'lat' => 'nullable',
            'lng' => 'nullable',
            'clean' => 'nullable',
            'dry_clean' => 'nullable',
            'clean_ironing' => 'nullable',
            'Commercial_cert' => 'nullable|image',
            'laundry_personal_id' => 'nullable|image',
            'shop_personal_id' => 'nullable|image',
        ];
        $validation = validator()->make($request->all(), $rules);

        if ($validation->fails()) {

            return responseJson('0', $validation->errors()->first());

        }

        if ($request->has('name') && $request->name != NULL) {
            $laundry->update($request->only('name'));
        }


        if ($request->has('password') && $request->password != NULL) {


            if (Hash::check($request->old_password, Auth::user()->password)) {
                $laundry->update(['password' => $request->password]);
            } else {
                return responseJson(0, trans('api.update.password_error'), null);
            }

        }

        if ($request->has('phone') && $request->phone != NULL) {
            $laundry->update($request->only('phone'));
        }


        if ($request->has('email') && $request->email != NULL) {

            $laundry->update($request->only('email'));
        }

        /*
=
            'clean_ironing'=>'nullable',*/
        if ($request->has('dry_clean') && $request->dry_clean != NULL) {

            $laundry->update($request->only('dry_clean'));
        }
        if ($request->has('clean_ironing') && $request->clean_ironing != NULL) {

            $laundry->update($request->only('clean_ironing'));
        }

        if ($request->has('clean') && $request->clean != NULL) {

            $laundry->update($request->only('clean'));
        }

        if ($request->has('lat') && $request->lat != NULL) {

            $laundry->update($request->only('lat'));
        }

        if ($request->has('lng') && $request->lng != NULL) {

            $laundry->update($request->only('lng'));
        }
        if ($request->has('mattresses') && $request->mattresses != NULL) {

            $laundry->update($request->only('mattresses'));
        }

        if ($request->has('carpets') && $request->carpets != NULL) {

            $laundry->update($request->only('carpets'));

        }
        if ($request->has('clothes') && $request->clothes != NULL) {

            $laundry->update($request->only('clothes'));

        }

        if ($request->has('Commercial_number') && $request->Commercial_number != NULL) {

            $laundry->update($request->only('Commercial_number'));
        }
        if ($request->has('address') && $request->address != NULL) {

            $laundry->update($request->only('address'));
        }
        if ($request->has('clothes') && $request->clothes != NULL) {

            $laundry->update($request->only('clothes'));
        }


        if ($request->hasFile('image') && $request->image != NULL) {
            $laundry->update(['image' => saveImage($request->image, '/laundries')]);

        }
        if ($request->hasFile('Commercial_cert') && $request->Commercial_cert != NULL) {
            $laundry->update(['Commercial_cert' => saveImage($request->Commercial_cert, '/laundries')]);

        }
        if ($request->hasFile('laundry_personal_id') && $request->laundry_personal_id != NULL) {
            $laundry->update(['laundry_personal_id' => saveImage($request->laundry_personal_id, '/laundries')]);
        }
        if ($request->hasFile('shop_personal_id') && $request->shop_personal_id != NULL) {
            $laundry->update(['shop_personal_id' => saveImage($request->shop_personal_id, '/laundries')]);
        }


        $laundry = Auth::user();
        return responseJson(1, trans('api.update.success'), ['laundry' => $laundry]);
    }


    public function verifyProfile(Request $request)
    {

        $rules =
            [
                'email' => 'required|email|exists:laundries,email',
                'verify_mobile' => 'required|exists:laundries,verify_mobile',
            ];
        $validation = validator()->make($request->all(), $rules);
        if ($validation->fails()) {
            return responseJson('0', $validation->errors()->first());
        }
        $laundry = Laundry::with('addresses')->where('email', $request->email)->where($request->only('verify_mobile'));
        if ($laundry == null) {
            return responseJson(1, trans('api.update.error'));
        }
        $laundry->update(['mobile_verified' => Carbon::now()]);
        $laundry = Laundry::where('email', $request->email)->where($request->only('verify_mobile'))->first();
        return responseJson(1, trans('api.update.success'), ['laundry' => $laundry]);
    }

    public function resend_verify(Request $request)
    {

        $rules = [
            'email' => 'required|email|exists:laundries,email',
        ];
        $validation = validator()->make($request->all(), $rules);
        if ($validation->fails()) {
            return responseJson('0', $validation->errors()->first());
        }
        $laundry = Laundry::where('email', $request->email)->first();
        if ($laundry == null) {
            return responseJson(1, trans('api.user.resend_code_error'));
        }
        //TODO :: add message phone verify

        return responseJson(0, trans('api.user.resend_code'), $laundry);
    }

    public function logout(Request $request)
    {
        $rules = [
            'token' => 'required|string|exists:tokens,token,model_id,' . Auth::id()
        ];
        $validation = validator()->make($request->all(), $rules);

        if ($validation->fails()) {
            return responseJson('0', $validation->errors()->first());
        }
        $token = Token::where('token', $request->token)->where('user_id', Auth::id())->delete();

        return responseJson(1, trans('api.logout'));
    }
}
