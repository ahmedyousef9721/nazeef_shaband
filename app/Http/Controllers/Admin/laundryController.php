<?php

namespace App\Http\Controllers\admin;

use App\Laundry;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class laundryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $laundries = Laundry::all();


        return view('admin.laundries.index', compact('laundries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.laundries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       /* $this->validate($request, [
            'name' => 'required|string|max:191',
            'email' => 'required|email|max:191|unique:laundries,email',
            'password' => 'required|string|confirmed',
            'image' => 'nullable|image',
            'balance' => 'nullable',
            'phone' => 'required|unique:laundries,phone',
            'salary' => 'nullable',
            'percent' => 'nullable'

        ]);*/
        $requests = $request->except('image');
        if ($request->hasFile('image')) {
            $requests['image'] = saveImage($request->image, 'laundry');
        }
        Laundry::create($requests);

        toast('تم الاضافه بنجاح', 'success', 'top-right');
        return redirect()->route('laundries.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $laundry = Laundry::findOrFail($id);

        return view('admin.laundries.edit', compact('laundry'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

       /* $this->validate($request, [
            'name' => 'required|string|max:191',
            'email' => 'required|email|max:191|unique:laundries,email',
            'password' => 'required|string|confirmed',
            'image' => 'nullable|image',
            'balance' => 'nullable',
            'phone' => 'required|unique:laundries,phone',
            'salary' => 'nullable',
            'percent' => 'nullable'

        ]);*/
        $requests = $request->except('image');
        if ($request->hasFile('image')) {
            $requests['image'] = saveImage($request->image, '/laundry');
        }
        Laundry::findOrFail($id)->fill($requests)->save();
        toast('تم التعديل بنجاح', 'success', 'top-right');
        return redirect()->route('laundries.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Laundry::destroy($id);
        toast('تم الحذف', 'success', 'top-right');
        return back();
    }

}
