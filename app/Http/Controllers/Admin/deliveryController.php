<?php

namespace App\Http\Controllers\admin;

use App\Delivery;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class deliveryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $deliveries = Delivery::all();


        return view('admin.deliveries.index', compact('deliveries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $users=User::all();
        return view('admin.deliveries.create',compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:191',
            'email' => 'required|email|max:191|unique:deliveries,email',
            'password' => 'required|string|confirmed',
            'image' => 'nullable|image',
            'balance' => 'nullable',
            'phone' => 'required|unique:deliveries,phone',
            'type' => 'required|string',
            'salary' => 'nullable',
            'percent' => 'nullable'

        ]);
        $requests = $request->except('image');
        if ($request->hasFile('image')) {
            $requests['image'] = saveImage($request->image, 'delivery');
        }
        Delivery::create($requests);

        toast('تم الاضافه بنجاح', 'success', 'top-right');
        return redirect()->route('deliveries.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $delivery = Delivery::findOrFail($id);
        $users=User::all();

        return view('admin.deliveries.edit', compact('delivery','users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name' => 'required|string|max:191',
            'email' => 'required|email|max:191|unique:deliveries,email',
            'password' => 'required|string|confirmed',
            'image' => 'nullable|image',
            'balance' => 'nullable',
            'phone' => 'required|unique:deliveries,phone',
            'type' => 'required|string',
            'salary' => 'nullable',
            'percent' => 'nullable'

        ]);
        $requests = $request->except('image');
        if ($request->hasFile('image')) {
            $requests['image'] = saveImage($request->image, '/delivery');
        }
        Delivery::findOrFail($id)->fill($requests)->save();
        toast('تم التعديل بنجاح', 'success', 'top-right');
        return redirect()->route('deliveries.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Delivery::destroy($id);
        toast('تم الحذف', 'success', 'top-right');
        return back();
    }

}
