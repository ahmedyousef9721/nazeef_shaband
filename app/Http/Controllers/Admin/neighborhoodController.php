<?php

namespace App\Http\Controllers\admin;

use App\Models\Area;
use App\Models\City;
use App\Models\Neighborhood;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class neighborhoodController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $neighborhoods = Neighborhood::all()->load('borders');


        return view('admin.neighborhoods.index', compact('neighborhoods'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities = City::all();
        return view('admin.neighborhoods.create', compact('cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name_ar' => 'required|string|max:191,unique:neighborhoods,name_ar',
            'name_en' => 'required|string|max:191,unique:neighborhoods,name_en',
            'city_id' => 'required|integer|exists:cities,id',
            'borders'=>'required',

        ]);
        $requests = $request->except('borders');
        $borders=[];
        foreach ($request->borders as $point) {
            $arr = explode(',', $point);
            if (count($arr) === 2) {
                $border = [
                    'lat' => $arr[0],
                    'lng' => $arr[1],
                ];
                $borders[] = $border;
            } else {
                return back()->withInput()->withErrors(['borders' => 'يوجد نقطه خطوط طول وعرض غير سليمه']);
            };
        }
        $neighorhood = Neighborhood::create($requests);
        $neighorhood->createMany($borders);
        toast('تم الاضافه بنجاح', 'success', 'top-right');
        return redirect()->route('neighborhoods.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $neighborhood = Neighborhood::findOrFail($id);
        $cities = City::all();

        return view('admin.neighborhoods.edit', compact('neighborhood', 'cities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name_ar' => 'required|string|max:191,unique:neighborhoods,name_ar,' . $id,
            'name_en' => 'required|string|max:191,unique:neighborhoods,name_en,' . $id,
            'city_id' => 'required|integer|exists:cities,id'


        ]);
        $requests = $request->except('borders');
        $borders=[];
        foreach ($request->borders as $point) {
            $arr = explode(',', $point);
            if (count($arr) === 2) {
                $border = [
                    'lat' => $arr[0],
                    'lng' => $arr[1],
                ];
                $borders[] = $border;
            } else {
                return back()->withInput()->withErrors(['borders' => 'يوجد نقطه خطوط طول وعرض غير سليمه']);
            };
        }
        $neighborhood=Neighborhood::findOrFail($id)->fill($requests)->save();
        $neighborhood->borders->delete();
        $neighborhood->borders()->createMany($borders);
        toast('تم التعديل بنجاح', 'success', 'top-right');
        return redirect()->route('neighborhoods.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Neighborhood::destroy($id);
        toast('تم الحذف', 'success', 'top-right');
        return back();
    }
}
