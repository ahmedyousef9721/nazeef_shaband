<?php

namespace App\Http\Controllers\admin;

use App\Client;
use App\Models\Address;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class addressController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $addresses = Address::all();


        return view('admin.addresses.index', compact('addresses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $clients = Client::all();
        return view('admin.addresses.create', compact('clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:191',
            'lat' => 'required|numeric|max:191',
            'lng' => 'required|numeric',
            'client_id' => 'required|integer|exists:clients,id'

        ]);

        Address::create($request->all());

        toast('تم الاضافه بنجاح', 'success', 'top-right');

        return redirect()->route('addresses.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $address = Address::findOrFail($id);

        $clients = Client::all();

        return view('admin.addresses.edit', compact('address','clients'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:191',
            'lat' => 'required|numeric|max:191',
            'lng' => 'required|numeric',
            'client_id' => 'required|integer|exists:clients,id'

        ]);


        $address = Address::findOrFail($id)->fill($request->all());
        $address->save();
        toast('تم التعديل بنجاح', 'success', 'top-right');
        return redirect()->route('addresses.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Address::destroy($id);
        toast('تم الحذف', 'success', 'top-right');
        return back();
    }
}
