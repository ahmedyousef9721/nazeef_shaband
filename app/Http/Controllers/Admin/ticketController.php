<?php

namespace App\Http\Controllers\admin;

use App\Client;
use App\Delivery;
use App\Http\Controllers\Controller;
use App\Laundry;
use App\Models\Ticket;
use Illuminate\Http\Request;

class ticketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tickets = Ticket::all();
        return view('admin.tickets.index', compact('tickets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = Client::all();
        $laundries = Laundry::all();
        $deliveries = Delivery::all();

        return view('admin.tickets.create', compact('clients', 'laundries', 'deliveries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            "title" => "required|string",
            "priority" => "required|string|in:low,normal,high",
            "status" => "required|string|in:open,closed",
            "model_type" => "required|string|in:App\Client,App\Delivery,App\Laundry",
            "model_id" => "required|integer",
            "message" => "required|string",
            "file" => "nullable",
        ]);
        $ticket_data = $request->except('message', 'file');
        $ticket_data['ticket_number'] = rand(000000, 999999);
        $message_data = [
            'message' => $request->message,
            'replier_type' => $request->model_type,
            'replier_id' => $request->model_id,
        ];
        if ($request->hasFile('file')) {
            $messag_data['file'] = saveImage($request->file('file'), 'tickets');
        }
        $ticket = Ticket::create($ticket_data);
        $ticket->messages()->create($message_data);
        toast('تم الاضافه بنجاح', 'success', 'top-right');

        return redirect()->route('tickets.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ticket $ticket
     * @return \Illuminate\Http\Response
     */
    public function show(ticket $ticket)
    {
        dd($ticket);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ticket $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ticket = Ticket::findOrFail($id);
        $clients = Client::all();
        $laundries = Laundry::all();
        $deliveries = Delivery::all();


        return view('admin.tickets.edit', compact('ticket', 'clients', 'laundries', 'deliveries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\ticket $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            "title" => "required|string",
            "priority" => "required|string|in:low,normal,high",
            "status" => "required|string|in:open,closed",
            "model_type" => "required|string|in:App\Client,App\Delivery,App\Laundry",
            "model_id" => "required|integer",
            "message" => "nullable",
            "file" => "nullable",
        ]);
        $ticket = Ticket::findOrFail($id);
        $ticket_data = $request->except('message', 'file');
        $ticket_data['ticket_number'] = rand(000000, 999999);
        $message_data = [
            'message' => $request->message,
            'replier_type' => $request->model_type,
            'replier_id' => $request->model_id,
        ];
        if ($request->hasFile('file')) {
            $messag_data['file'] = saveImage($request->file('file'), 'tickets');
        }
        $ticket->fill($ticket_data);
        $ticket->messages()->create($message_data);
        $ticket->save();
        toast('تم الاضافه بنجاح', 'success', 'top-right');
        return redirect()->route('tickets.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ticket $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Ticket::destroy($id);
        toast('تم الحذف بنجاح', 'success', 'top-right');

        return back();
    }
}
