<?php

namespace App\Http\Controllers\admin;

use App\Models\Area;
use App\Models\Border;
use App\Models\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class borderController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     *
     * @return Response
     */
    public function index()
    {
        $Borders = Border::all();


        return view('admin.Borders.index', compact('Borders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities=City::all();

        return view('admin.Borders.create',compact('cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name_ar' => 'required|string|max:191,unique:Borders,name_ar',
            'name_en' => 'required|string|max:191,unique:Borders,name_en',
            'city_id'=>'required|integer|exists:cities,id'

        ]);
        $requests = $request->all();
        Border::create($requests);

        toast('تم الاضافه بنجاح', 'success', 'top-right');
        return redirect()->route('Borders.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Border = Border::findOrFail($id);
        $cities=City::all();

        return view('admin.Borders.edit', compact('Border','cities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name_ar' => 'required|string|max:191,unique:Borders,name_ar,'.$id,
            'name_en' => 'required|string|max:191,unique:Borders,name_en,'.$id,
            'city_id'=>'required|integer|exists:cities,id'


        ]);
        $requests = $request->all();
        Border::findOrFail($id)->fill($requests)->save();

        toast('تم التعديل بنجاح', 'success', 'top-right');
        return redirect()->route('Borders.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Border::destroy($id);
        toast('تم الحذف', 'success', 'top-right');
        return back();
    }
}
