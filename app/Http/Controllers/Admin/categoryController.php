<?php

namespace App\Http\Controllers\admin;

use App\Models\category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class categoryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $categories = Category::all();


        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name_ar' => 'required|string|max:191,unique:categories,name_ar',
            'name_en' => 'required|string|max:191,unique:categories,name_en',
            'image' => 'required|image',
            'type' => 'required|string|in:clothes,carpets,mattresses',
            'clean_price' => 'required|numeric',
            'dry_clean_price' => 'required|numeric',
            'clean_ironing_price' => 'required|numeric',
            'fast_clean_price'=>'required_if:type,clothes',
            'fast_dry_clean_price'=>'required_if:type,clothes',
            'fast_clean_ironing_price'=>'required_if:type,clothes'

        ]);
        $requests = $request->except('image');
        $requests['image']=saveImage($request->image,'categories');
        Category::create($requests);

        toast('تم الاضافه بنجاح', 'success', 'top-right');
        return redirect()->route('categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);

        return view('admin.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name_ar' => 'required|string|max:191,unique:categories,name_ar',
            'name_en' => 'required|string|max:191,unique:categories,name_en',
            'image' => 'nullable|image',
            'type' => 'required|string|in:clothes,carpets,mattresses',
            'clean_price' => 'required|numeric',
            'dry_clean_price' => 'required|numeric',
            'clean_ironing_price' => 'required|numeric',
            'fast_clean_price'=>'required_if:type,clothes',
            'fast_dry_clean_price'=>'required_if:type,clothes',
            'fast_clean_ironing_price'=>'required_if:type,clothes'

        ]);
        $requests = $request->except('image');
        if($request->has('image')){
        $requests['image']=saveImage($request->image,'categories');
        }
        Category::findOrFail($id)->fill($requests)->save();

        toast('تم التعديل بنجاح', 'success', 'top-right');
        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::destroy($id);
        toast('تم الحذف', 'success', 'top-right');
        return back();
    }
}
