<?php

namespace App\Http\Controllers\admin;

use App\Models\Area;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class areaController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $areas = Area::all();


        return view('admin.areas.index', compact('areas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.areas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name_ar' => 'required|string|max:191,unique:areas,name_ar',
            'name_en' => 'required|string|max:191,unique:areas,name_en',
            'borders' => 'required|array',


        ]);
        $requests = $request->except('borders');

        $borders = [];
        foreach ($request->borders as $point) {
            $arr = explode(',', $point);
            if (count($arr) === 2) {
                $border = [
                    'lat' => $arr[0],
                    'lng' => $arr[1],
                ];
                $borders[] = $border;
            } else {
                return back()->withInput()->withErrors(['borders' => 'يوجد نقطه خطوط طول وعرض غير سليمه']);
            };
        }
        $area = Area::create($requests);
        $area->borders()->createMany($borders);
        toast('تم الاضافه بنجاح', 'success', 'top-right');
        return redirect()->route('areas.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $area = Area::findOrFail($id);
        return view('admin.areas.edit', compact('area'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name_ar' => 'required|string|max:191,unique:areas,name_ar,' . $id,
            'name_en' => 'required|string|max:191,unique:areas,name_en,' . $id,

        ]);
        $requests = $request->except('borders');

        $borders = [];
        foreach ($request->borders as $point) {
            $arr = explode(',', $point);
            if (count($arr) === 2) {
                $border = [
                    'lat' => $arr[0],
                    'lng' => $arr[1],
                ];
                $borders[] = $border;
            } else {
                return back()->withInput()->withErrors(['borders' => 'يوجد نقطه خطوط طول وعرض غير سليمه']);
            };
        }
        $area = Area::findOrFail($id)->fill($requests);
        $area->borders->delete();
        $area->borders()->createMany($borders);
        $area->save();
        toast('تم التعديل بنجاح', 'success', 'top-right');
        return redirect()->route('areas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Area::destroy($id);
        toast('تم الحذف', 'success', 'top-right');
        return back();
    }
}
