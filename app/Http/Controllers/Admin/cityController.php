<?php

namespace App\Http\Controllers\admin;

use App\Models\Area;
use App\Models\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class cityController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $cities = City::all();


        return view('admin.cities.index', compact('cities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $areas = Area::all();
        return view('admin.cities.create', compact('areas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name_ar' => 'required|string|max:191,unique:cities,name_ar',
            'name_en' => 'required|string|max:191,unique:cities,name_en',
            'area_id' => 'required|integer|exists:areas,id',
            'borders' => 'required'


        ]);
        $requests = $request->except('borders');
        foreach ($request->borders as $point) {
            $arr = explode(',', $point);
            if (count($arr) === 2) {
                $border = [
                    'lat' => $arr[0],
                    'lng' => $arr[1],
                ];
                $borders[] = $border;
            } else {
                return back()->withInput()->withErrors(['borders' => 'يوجد نقطه خطوط طول وعرض غير سليمه']);
            };
        }
        $city = City::create($requests);

        $city->borders()->createMany($borders);

        toast('تم الاضافه بنجاح', 'success', 'top-right');
        return redirect()->route('cities.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $city = City::findOrFail($id);
        $areas = Area::all();

        return view('admin.cities.edit', compact('city', 'areas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name_ar' => 'required|string|max:191,unique:cities,name_ar,' . $id,
            'name_en' => 'required|string|max:191,unique:cities,name_en,' . $id,
            'area_id' => 'required|integer|exists:areas,id',
            'borders' => 'required'


        ]);
        $requests = $request->except('borders');
        $borders = [];
        foreach ($request->borders as $point) {
            $arr = explode(',', $point);
            if (count($arr) === 2) {
                $border = [
                    'lat' => $arr[0],
                    'lng' => $arr[1],
                ];
                $borders[] = $border;
            } else {
                return back()->withInput()->withErrors(['borders' => 'يوجد نقطه خطوط طول وعرض غير سليمه']);
            };
        }
        $city = City::findOrFail($id)->fill($requests);
        $city->borders->delete();
        $city->borders()->createMany($borders);
        $city->save();

        toast('تم التعديل بنجاح', 'success', 'top-right');
        return redirect()->route('cities.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        City::destroy($id);
        toast('تم الحذف', 'success', 'top-right');
        return back();
    }
}
