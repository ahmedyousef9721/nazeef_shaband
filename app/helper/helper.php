<?php


//Json array response
function responseJson($status, $msg, $data = null)
{
    $response = [
        'status' => (int) $status,
        'message' => $msg,
        'data' => $data,
    ];
    return response()->json($response);
}


function saveImage($file, $folder='/')
{
    $fileName = $file->getClientOriginalName();
    $dest = public_path('uploads/'.$folder);
    $file->move($dest, $fileName);
    return 'uploads/'.$folder . '/' . $fileName;
}
