function toggleFastOrder(type) {
    var fast_element = $('.fastOrder');

    if (type != 'clothes') {

        fast_element.addClass('d-none');
        fast_element.removeClass('d-block')
    } else {
        fast_element.removeClass('d-none');
        fast_element.addClass('d-block')

    }

}

function toggleSalariesInputs(type) {

    /*'independent'=>'مستقل','company'=>'تابع لشركه','website'*/
    if (type === 'independent') {
        $('.percent-group').addClass('d-block');
        $('.salary-group').removeClass('d-none');

        $('.company-group').addClass('d-none');
        $('.company-group').removeClass('d-block');


        $('.salary-group').addClass('d-none');
        $('.percent-group').removeClass('d-block');
    } else if (type === 'company') {
        $('.percent-group').addClass('d-block');
        $('.salary-group').removeClass('d-none');

        $('.company-group').addClass('d-none');
        $('.company-group').removeClass('d-block');


        $('.salary-group').addClass('d-none');
        $('.percent-group').removeClass('d-block');


    } else {

        $('.percent-group').removeClass('d-block');
        $('.salary-group').addClass('d-none');


        $('.company-group').removeClass('d-none');
        $('.company-group').addClass('d-block');


        $('.salary-group').removeClass('d-none');
        $('.percent-group').addClass('d-block');

    }

}


/*
function change_options() {

    for (part in parts) {
        var val = parts[part];

        var options = $('option[data-selectPart="' + part + '"]');
        options.val(val);
        options.html(val);
    }

}
*/


var demo3 = function () {
    var map = new GMaps({
        div: '#m_gmap_3',
        lat: -51.38739,
        lng: -6.187181,
    });
    map.addMarker({
        lat: -51.38739,
        lng: -6.187181,
        draggable: true,
        dragend: function (e) {
            console.log(e);
        }

    });
    /* map.addMarker({
         lat: -12.042,
         lng: -77.028333,

         draggable: true,

         title: 'Marker with InfoWindow',
         infoWindow: {
             content: '<span style="color:#000">HTML Content!</span>'
         }
     });*/
    map.setZoom(5);
}

$('.select2').select2();

$('.taggable-select2').select2({
    tags: true,


});


function getClientByType(type) {
    var members = types[type];
    var select_name = 'select[name="model_id"]';
    addOptionToSelect(select_name, members)
}

function addOptionToSelect(el, options) {

    var opt = '';
    $.each(options, function (value, text) {
        opt += '<option value="' + value + '">' + text + '</option>'
    });
    $(el).html(opt);
}


var map;
if( lat===undefined ||lat ===null){
   var lat=24.793274529734642;

}if( lng===undefined ||lng ===null){
   var lng=46.72363562011719;

}
function initialize() {
    var mapOptions = {
        zoom: 14,
        center: {lat: lat, lng: lng}
    };
    map = new google.maps.Map(document.getElementById('map'),
        mapOptions);

    var marker = new google.maps.Marker({
        // The below line is equivalent to writing:
        // position: new google.maps.LatLng(-34.397, 150.644)
        position: {lat: lat, lng: lng},
        map: map,
        draggable: true

    });



    google.maps.event.addListener(marker, 'dragend', function(event){


        document.querySelector("input[name='lng']").value=event.latLng.lng();


        document.querySelector("input[name='lat']").value=event.latLng.lat();


        /*  document.getElementById('current').innerHTML = '<p>Marker dropped: Current Lat: ' + evt.latLng.lat().toFixed(3) + ' Current Lng: ' + evt.latLng.lng().toFixed(3) + '</p>'*/;
    });

    /*google.maps.event.addListener(myMarker, 'dragstart', function(evt){
        document.getElementById('current').innerHTML = '<p>Currently dragging marker...</p>';
    });*/
}
if(document.getElementById('map')){
google.maps.event.addDomListener(window, 'load', initialize);
}
