<!--begin: Datatable -->
<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
    <thead>
    <tr>
        <th>التسلسل</th>
        <th>الاسم بالعربيه</th>
        <th>الاسم بالانجليزيه</th>
        <th>المدينه</th>

        <th>الاعدادت</th>
    </tr>
    </thead>
    <tbody>
    @foreach($neighborhoods as $neighborhood)
        <tr>
            <td>{!!$loop->iteration!!}</td>
           
            <td>{!!$neighborhood->name_ar!!}</td>
            <td>{!!$neighborhood->name_en!!}</td>
            <td>{!!$neighborhood->city->name_ar!!}</td>
            
            <td>
                <a href="{!!route('neighborhoods.edit',$neighborhood->id)!!}" class="btn btn-primary">
                    <i class="fas fa-pen"></i>
                    تعديل
                </a>

                <form method="POST" action="{!!route('neighborhoods.destroy',$neighborhood->id)!!}">
                    @csrf()
                     @method('delete')
                    <button type="submit" class="btn btn-danger">
                        <i class="fas fa-trash"></i>
                        حذف
                    </button>
                </form>
            </td>
        </tr>


    @endforeach

    </tbody>

</table>


<div id="map-polygon" style="width: 100%; height: 500px"></div>
@push('scripts')
<script>

    // This example creates a simple polygon representing the Bermuda Triangle.

    var neigborhoods={!! $neighborhoods->toJson() !!}
    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 5,
            center: {lat: 24.886, lng: -70.268},
            mapTypeId: 'terrain'
        });

        // Define the LatLng coordinates for the polygon's path.
        var triangleCoords = [
            {lat: 25.774, lng: -80.190},
            {lat: 18.466, lng: -66.118},
            {lat: 32.321, lng: -64.757},
            {lat: 25.774, lng: -80.190}
        ];

        // Construct the polygon.
        var bermudaTriangle = new google.maps.Polygon({
            paths: triangleCoords,
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35
        });
        bermudaTriangle.setMap(map);
    }
</script>
@endpush
