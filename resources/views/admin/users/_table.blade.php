<!--begin: Datatable -->
<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
    <thead>
    <tr>
        <th>#</th>
        <th>الصوره</th>
        <th>الاسم</th>
        <th>الهاتف</th>
        <th>البريد الالكترونى</th>
        <th>النوع</th>
        <th>الاعدادت</th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td>{!!$loop->iteration!!}</td>
            <td><img src="{!!asset($user->image)!!}" height="100" width="100" /> </td>
            <td>{!!$user->name!!}</td>
            <td>{!!$user->phone!!}</td>
            <td>{!!$user->email!!}</td>
            <td>{!!$user->type!!}</td>
            <td>
                <a href="{!!route('users.edit',$user->id)!!}" class="btn btn-primary"> <i class="fas fa-pen"></i>  تعديل</a>

                <form method="POST" action="{!!route('users.destroy',$user->id)!!}">
                    @csrf()
                     @method('delete')
                    <button type="submit" class="btn btn-danger">
                        <i class="fas fa-trash"></i>
                        حذف
                    </button>
                </form>
            </td>

        </tr>
    @endforeach
    </tbody>

</table>
