<div class="m-portlet__body">

    <div class="form-group m-form__group">
        <label for="exampleInputName">الاسم</label>

        {!! Form::text('name',null,['class'=>'form-control m-input','id'=>'exampleInputName','placeholder'=>'ادخل الاسم'])!!}
    </div>

    <div class="form-group m-form__group">
        <label for="exampleInputEmail1">البريد الالكترونى</label>

        {!! Form::email('email',null,['class'=>'form-control m-input','id'=>'exampleInputEmail1','placeholder'=>'ادخل البريد الالكترونى'])!!}
    </div>

    <div class="form-group m-form__group">
        <label for="exampleInputEmail1">الهاتف</label>

        {!! Form::text('phone',null,['class'=>'form-control m-input','id'=>'exampleInputEmail1','placeholder'=>'رقم الهاتف'])!!}
    </div>

    <div class="form-group m-form__group">
        <label for="exampleInputEmail1">الرصيد</label>

        {!! Form::number('balance',null,['class'=>'form-control m-input','id'=>'exampleInputEmail1','placeholder'=>'الرصيد'])!!}
    </div>


    <div class="m-form__group form-group row">
        <label class="col-12 col-form-label">الحساب مفعل</label>
        <div class="col-12">
			<span class="m-switch m-switch--info">
			<label>
                {!! Form::checkbox('active') !!}
                <span></span>
				</label>
				</span>
        </div>

    </div>

    <div class="form-group m-form__group">
        <label for="exampleInputPassword1">كلمه المرور</label>


        {!! Form::password('password',['class'=>'form-control m-input','id'=>'exampleInputPassword1','placeholder'=>'ادخل كلمه المرور'])!!}

    </div>

    <div class="form-group m-form__group">
        <label for="exampleInputPassword2"> تأكيد كلمه المرور </label>

        {!! Form::password('password_confirmation',['class'=>'form-control m-input','id'=>'exampleInputPassword2','placeholder'=>'ادخل تأكيد كلمه المرور'])!!}
    </div>


    <div class="form-group m-form__group">
        @if(isset($user))
            <img src={!!asset($user->image)!!} width="100" height="100">
        @endif
        <label for="imageInput"> الصوره </label>
        <input type="file" class="form-control m-input" name="image" id="imageInput">
    </div>
</div>
