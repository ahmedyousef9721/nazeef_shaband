<!--begin: Datatable -->
<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
    <thead>
    <tr>
        <th>#</th>
        <th>الصوره</th>
        <th>الاسم</th>
        <th>الهاتف</th>
        <th>البريد الالكترونى</th>
        <th>الرصيد</th>
        <th>الاعدادت</th>
    </tr>
    </thead>
    <tbody>
    @foreach($clients as $client)
        <tr>
            <td>{!!$loop->iteration!!}</td>
            <td>
                <img src="{!!asset($client->image)!!}" height="100" width="100" />
            </td>
            <td>{!!$client->name!!}</td>
            <td>{!!$client->phone!!}</td>
            <td>{!!$client->email!!}</td>
            <td>{!!$client->balane!!}</td>
            <td>
                <a href="{!!route('clients.edit',$client->id)!!}" class="btn btn-primary">
                    <i class="fas fa-pen"></i>
                    تعديل</a>

                <form method="POST" action="{!!route('clients.destroy',$client->id)!!}">
                    @csrf()
                     @method('delete')
                    <button type="submit" class="btn btn-danger">
                        <i class="fas fa-trash"></i>
                        حذف
                    </button>
                </form>
            </td>
        </tr>


    @endforeach

    </tbody>

</table>
