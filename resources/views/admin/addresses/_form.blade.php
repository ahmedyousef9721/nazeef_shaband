<div class="m-portlet__body">

    <div class="form-group m-form__group">
        <label for="exampleInputName">الاسم بالعربيه</label>

        {!! Form::text('name',null,['class'=>'form-control m-input','id'=>'exampleInputName','placeholder'=>'ادخل الاسم بالعربيه'])!!}
    </div>

    <div class="form-group m-form__group">
        <label for="exampleInputEmail1">العميل</label>

        {!! Form::select('client_id',$clients->pluck('name','id'),null,['class'=>'form-control m-input','id'=>'exampleInputEmail1','placeholder'=>'ادخل الاسم بالانجليزيه'])!!}
    </div>

    {!! Form::hidden('lat',null)!!}
    {!! Form::hidden('lng',null)!!}
    <div id='map' style="width: 100%; height: 500px;"></div>
</div>
@if(isset($address))
   <script>
       var lat={!! $address->lat !!}
       var lng={!! $address->lat !!}


   </script>

    @endif
