<!--begin: Datatable -->
<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
    <thead>
    <tr>
        <th>التسلسل</th>
        <th>العميل</th>
        <th>العنوان</th>
        {{--<th>الخريطه</th>--}}

        <th>الاعدادت</th>
    </tr>
    </thead>
    <tbody>
    @foreach($addresses as $address)
        <tr>
            <td>{!!$loop->iteration!!}</td>
           
            <td>{!!optional($address->client)->name!!}</td>
            <td>{!!$address->name!!}</td>
{{--
            <td><div class="map" style="width: 200px;height: 200px"></div></td>
--}}

            <td>
                <a href="{!!route('addresses.edit',$address->id)!!}" class="btn btn-primary">
                    <i class="fas fa-pen"></i>
                    تعديل</a>

                <form method="POST" action="{!!route('addresses.destroy',$address->id)!!}">
                    @csrf()
                     @method('delete')
                    <button type="submit" class="btn btn-danger">
                        <i class="fas fa-trash"></i>
                        حذف
                    </button>
                </form>
            </td>
        </tr>


    @endforeach

    </tbody>

</table>
