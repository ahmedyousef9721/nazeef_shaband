<!--begin: Datatable -->
<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
    <thead>
    <tr>
        <th>التسلسل</th>
        <th>العنوان</th>
        <th>العضو</th>
        <th>الحاله</th>
        <th>الاهميه</th>
        <th>صاحب التذكره</th>
        <th>التاريخ</th>
        <th>اخر رساله</th>
        <th>الاعدادت</th>
    </tr>
    </thead>
    <tbody>
    @foreach($tickets as $ticket)
        <tr>
            <td>{!!$loop->iteration!!}</td>
            <td>{!!$ticket->title!!}</td>
            <td>{!!optional($ticket->client)->name!!}</td>
            <td>{!!$ticket->status!!}</td>
            <td>{!!$ticket->priority!!}</td>
            <td>{!!optional($ticket->client)->name!!}</td>
            <td>{!!optional($ticket->created_at)->format('Y-m-d')!!}</td>
            <td>{!!optional(optional($ticket->messages)->last())->message!!}</td>
            <td>
                <a href="{!!route('tickets.edit',$ticket->id)!!}" class="btn btn-primary">
                    <i class="fas fa-pen"></i>
                    تعديل</a>

                <form method="POST" action="{!!route('tickets.destroy',$ticket->id)!!}">
                    @csrf()
                     @method('delete')
                    <button type="submit" class="btn btn-danger">
                        <i class="fas fa-trash"></i>
                        حذف
                    </button>
                </form>
            </td>
        </tr>


    @endforeach

    </tbody>

</table>
