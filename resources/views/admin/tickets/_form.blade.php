<div class="m-portlet__body">

    <div class="form-group m-form__group">
        <label for="exampleInputName">الموضوع</label>

        {!! Form::text('title',null,['class'=>'form-control m-input','id'=>'exampleInputName','placeholder'=>'ادخل الموضوع'])!!}
    </div>

    <div class="form-group m-form__group">
        <label for="exampleInputEmail1">الاهميه</label>

        {!! Form::select('priority',['low'=>'low','normal'=>'normal','important'=>'important'],null,['class'=>'form-control m-input select2','id'=>'exampleInputEmail1','placeholder'=>' الاهميه'])!!}
    </div>
    <div class="form-group m-form__group">
        <label for="exampleInpu">الحاله</label>

        {!! Form::select('status',['closed'=>'مغلقه','open'=>'مفتوحه'],null,['class'=>'form-control m-input select2','id'=>'exampleInpu','placeholder'=>'ادخل الحاله'])!!}
    </div>

    <div class="form-group m-form__group">
        <label for="exampl">نوع العضويه</label>

        {!! Form::select('model_type',['App\Client'=>'عميل','App\laundry'=>'مغسله','App\Delivery'=>'مندوب'],null,['class'=>'form-control m-input select2','id'=>'exampl','placeholder'=>'نوع العضويه','onChange'=>'getClientByType(this.value)'])!!}
    </div>

    <div class="form-group m-form__group">
        <label for="exa">العضو</label>

        {!! Form::select('model_id',
        (isset($ticket))?$ticket->model_type::get()->pluck('name','id'):[]
        ,null,['class'=>'form-control m-input select2','id'=>'exa','placeholder'=>'ادخل العضو'])!!}
    </div>
@if(!isset($ticket))
    <div class="form-group m-form__group">
        <label>الرساله</label>

        {!! Form::textarea('message',null,['class'=>'form-control m-input ','placeholder'=>'ادخل العضو'])!!}
    </div>
    <div class="form-group m-form__group">
        <label>الرساله</label>

        {!! Form::file('file',null,['class'=>'form-control m-input ','placeholder'=>'ادخل العضو'])!!}
    </div>
    @else
    @foreach($ticket->messages as $message)
<h3>الرسائل السابقه </h3>
        <hr>
            <div class="form-group m-form__group">
                <label>الرساله</label>
<textarea disabled="" class="form-control">{!! $message->message !!}</textarea>
            </div>
        @if($message->file)
            <div class="form-group m-form__group">
                <label>الملفات المرفقه</label>
<a href="{!! asset($message->file) !!}" download=""> <i class="fas fa-cloud-download"></i></a>
            </div>
            @endif

        @endforeach


    <h3>اضافه رساله جديده</h3>

        <div class="form-group m-form__group">
            <label>الرساله</label>

            {!! Form::textarea('message',null,['class'=>'form-control m-input ','placeholder'=>'ادخل العضو'])!!}
        </div>
        <div class="form-group m-form__group">
            <label>الرساله</label>

            {!! Form::file('file',null,['class'=>'form-control m-input ','placeholder'=>'ادخل العضو'])!!}
        </div>
    @endif

</div>

@push('scripts')

    <script>

        var types = {
            "App\\Client":{!! $clients->pluck('name','id') !!},
            'App\\laundry': {!! $laundries->pluck('name','id') !!},
            'App\\Delivery':   {!! $deliveries->pluck('name','id') !!}
        };

    </script>

@endpush
