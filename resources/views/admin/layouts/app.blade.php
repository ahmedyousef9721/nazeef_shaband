<!DOCTYPE html>

<html lang="ar" dir="rtl">

<!-- begin::Head -->

<head>
    <meta charset="utf-8" />
    <title>{!!env('APP_NAME')!!} |@yield('title')</title>
    <meta name="description" content="Blank inner page examples">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>

    <!--end::Web font -->

    <!--begin:: Global Mandatory Vendors -->
    <link href="{!!asset('dashboard')!!}/vendors/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />

    <!--end:: Global Mandatory Vendors -->

    <!--begin:: Global Optional Vendors -->
    <link href="{!!asset('dashboard')!!}/vendors/tether/dist/css/tether.css" rel="stylesheet" type="text/css" />
    <link href="{!!asset('dashboard')!!}/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet"
        type="text/css" />
    <link href="{!!asset('dashboard')!!}/vendors/bootstrap-datetime-picker/css/bootstrap-datetimepicker.min.css" rel="stylesheet"
        type="text/css" />
    <link href="{!!asset('dashboard')!!}/vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet"
        type="text/css" />
    <link href="{!!asset('dashboard')!!}/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
    <link href="{!!asset('dashboard')!!}/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css" rel="stylesheet"
        type="text/css" />
    <link href="{!!asset('dashboard')!!}/vendors/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css" rel="stylesheet"
        type="text/css" />
    <link href="{!!asset('dashboard')!!}/vendors/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
    <link href="{!!asset('dashboard')!!}/vendors/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="{!!asset('dashboard')!!}/vendors/nouislider/distribute/nouislider.css" rel="stylesheet" type="text/css" />
    <link href="{!!asset('dashboard')!!}/vendors/owl.carousel/dist/assets/owl.carousel.css" rel="stylesheet" type="text/css" />
    <link href="{!!asset('dashboard')!!}/vendors/owl.carousel/dist/assets/owl.theme.default.css" rel="stylesheet" type="text/css" />
    <link href="{!!asset('dashboard')!!}/vendors/ion-rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css" />
    <link href="{!!asset('dashboard')!!}/vendors/ion-rangeslider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet"
        type="text/css" />
    <link href="{!!asset('dashboard')!!}/vendors/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css" />
    <link href="{!!asset('dashboard')!!}/vendors/summernote/dist/summernote.css" rel="stylesheet" type="text/css" />
    <link href="{!!asset('dashboard')!!}/vendors/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet"
        type="text/css" />
    <link href="{!!asset('dashboard')!!}/vendors/animate.css/animate.css" rel="stylesheet" type="text/css" />
    <link href="{!!asset('dashboard')!!}/vendors/toastr/build/toastr.css" rel="stylesheet" type="text/css" />
    <link href="{!!asset('dashboard')!!}/vendors/jstree/dist/themes/default/style.css" rel="stylesheet" type="text/css" />
    <link href="{!!asset('dashboard')!!}/vendors/morris.js/morris.css" rel="stylesheet" type="text/css" />
    <link href="{!!asset('dashboard')!!}/vendors/chartist/dist/chartist.min.css" rel="stylesheet" type="text/css" />
    <link href="{!!asset('dashboard')!!}/vendors/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet" type="text/css" />
    <link href="{!!asset('dashboard')!!}/vendors/socicon/css/socicon.css" rel="stylesheet" type="text/css" />
    <link href="{!!asset('dashboard')!!}/vendors/vendors/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css" />
    <link href="{!!asset('dashboard')!!}/vendors/vendors/flaticon/css/flaticon.css" rel="stylesheet" type="text/css" />
    <link href="{!!asset('dashboard')!!}/vendors/vendors/metronic/css/styles.css" rel="stylesheet" type="text/css" />
    <link href="{!!asset('dashboard')!!}/vendors/vendors/fontawesome5/css/all.min.css" rel="stylesheet" type="text/css" />

    <!--end:: Global Optional Vendors -->


    <link href="{!!asset('dashboard')!!}/assets/demo/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />

    <link href="{!!asset('dashboard')!!}/assets/vendors/custom/datatables/datatables.bundle.rtl.css" rel="stylesheet" type="text/css" />
    <link href="{!!asset('dashboard/assets/style.css')!!}" rel="stylesheet" type="text/css" />

    <!--end::Global Theme Styles -->
    <link rel="shortcut icon" href="{!!asset('dashboard')!!}/assets/demo/media/img/logo/favicon.ico" />

    @stack('header')
</head>

<!-- end::Head -->

<!-- begin::Body -->

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

    <!-- begin:: Page -->
    <div class="m-grid m-grid--hor m-grid--root m-page">

        @include('admin.layouts._navbar')

        <!-- END: Header -->

        <!-- begin::Body -->
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

            <!-- BEGIN: Left Aside -->
            @include('admin.layouts._sidebar')

            <!-- END: Left Aside -->
            <div class="m-grid__item m-grid__item--fluid m-wrapper">

                <!-- BEGIN: Subheader -->
                <div class="m-subheader ">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <h3 class="m-subheader__title m-subheader__title--separator">@yield('title')</h3>
                            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">


                                <li class="m-nav__item m-nav__item--home">
                                    <a href="{!!route('admin.dashboard')!!}" class="m-nav__link m-nav__link--icon">
                                        <i class="m-nav__link-icon la la-home"></i>
                                    </a>
                                </li>
@yield('breadcrumb')

                            </ul>
                        </div>
                        <div>
                        </div>
                    </div>


                </div>

                <!-- END: Subheader -->

                <div class="m-content">
                    <!-- /resources/views/post/create.blade.php -->


                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                <!-- Create Post Form -->
                    @yield('content')
                </div>
            </div>
        </div>

        <!-- end:: Body -->

        <!-- begin::Footer -->
        @include('admin.layouts._footer')
        <!-- end::Footer -->
    </div>

    <!-- end:: Page -->

    <!--begin:: Global Mandatory Vendors -->
    <script src="{!!asset('dashboard')!!}/vendors/jquery/dist/jquery.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/popper.js/dist/umd/popper.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/js-cookie/src/js.cookie.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/moment/min/moment.min.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/wnumb/wNumb.js" type="text/javascript"></script>

    <!--end:: Global Mandatory Vendors -->

    <!--begin:: Global Optional Vendors -->
    <script src="{!!asset('dashboard')!!}/vendors/jquery.repeater/src/lib.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/jquery.repeater/src/jquery.input.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/jquery.repeater/src/repeater.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/jquery-form/dist/jquery.form.min.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/block-ui/jquery.blockUI.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/js/framework/components/plugins/forms/bootstrap-datepicker.init.js"
        type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/js/framework/components/plugins/forms/bootstrap-timepicker.init.js"
        type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/js/framework/components/plugins/forms/bootstrap-daterangepicker.init.js"
        type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/bootstrap-maxlength/src/bootstrap-maxlength.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/bootstrap-switch/dist/js/bootstrap-switch.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/js/framework/components/plugins/forms/bootstrap-switch.init.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js"
        type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/select2/dist/js/select2.full.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/typeahead.js/dist/typeahead.bundle.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/handlebars/dist/handlebars.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/inputmask/dist/jquery.inputmask.bundle.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/inputmask/dist/inputmask/inputmask.date.extensions.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/inputmask/dist/inputmask/inputmask.numeric.extensions.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/inputmask/dist/inputmask/inputmask.phone.extensions.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/nouislider/distribute/nouislider.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/owl.carousel/dist/owl.carousel.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/autosize/dist/autosize.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/clipboard/dist/clipboard.min.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/ion-rangeslider/js/ion.rangeSlider.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/dropzone/dist/dropzone.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/summernote/dist/summernote.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/markdown/lib/markdown.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/js/framework/components/plugins/forms/bootstrap-markdown.init.js"
        type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/js/framework/components/plugins/forms/jquery-validation.init.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/bootstrap-notify/bootstrap-notify.min.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/js/framework/components/plugins/base/bootstrap-notify.init.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/toastr/build/toastr.min.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/jstree/dist/jstree.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/raphael/raphael.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/morris.js/morris.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/chartist/dist/chartist.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/chart.js/dist/Chart.bundle.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/js/framework/components/plugins/charts/chart.init.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js"
        type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/vendors/jquery-idletimer/idle-timer.min.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/waypoints/lib/jquery.waypoints.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/counterup/jquery.counterup.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/es6-promise-polyfill/promise.min.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
    <script src="{!!asset('dashboard')!!}/vendors/js/framework/components/plugins/base/sweetalert2.init.js" type="text/javascript"></script>

    <!--end:: Global Optional Vendors -->

    <!--begin::Global Theme Bundle -->
    <script src="{!!asset('dashboard')!!}/assets/demo/base/scripts.bundle.js" type="text/javascript"></script>

    		<!--begin::Page Vendors -->
		<script src="{!!asset('dashboard')!!}/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>

		<!--end::Page Vendors -->

		<!--begin::Page Scripts -->
		<script src="{!!asset('dashboard')!!}/assets/demo/custom/crud/datatables/extensions/buttons.js" type="text/javascript"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBmzkCES6yiFCSpJyUR5gHuHNhxbGyCZ64
"></script>
    @include('sweetalert::alert')
    @stack('scripts')
    <script src="{!!asset('dashboard/assets/scripts.js')!!}" type="text/javascript"></script>

    <!--end::Global Theme Bundle -->
</body>

<!-- end::Body -->

</html>
