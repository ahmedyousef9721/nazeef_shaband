<!--begin: Datatable -->
<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
    <thead>
    <tr>
        <th>التسلسل</th>
        <th>الاسم بالعربيه</th>
        <th>الاسم بالانجليزيه</th>
        <th>المنطقه</th>

        <th>الاعدادت</th>
    </tr>
    </thead>
    <tbody>
    @foreach($cities as $city)
        <tr>
            <td>{!!$loop->iteration!!}</td>
           
            <td>{!!$city->name_ar!!}</td>
            <td>{!!$city->name_en!!}</td>
            <td>{!!$city->area->name_ar!!}</td>
            
            <td>
                <a href="{!!route('cities.edit',$city->id)!!}" class="btn btn-primary">
                    <i class="fas fa-pen"></i>
                    تعديل
                </a>

                <form method="POST" action="{!!route('cities.destroy',$city->id)!!}">
                    @csrf()
                     @method('delete')
                    <button type="submit" class="btn btn-danger">
                        <i class="fas fa-trash"></i>
                        حذف
                    </button>
                </form>
            </td>
        </tr>


    @endforeach

    </tbody>

</table>
