<!DOCTYPE html>

<html lang="ar" dir="rtl">

	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title></title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>

		<!--end::Web font -->

		<!--begin:: Global Mandatory Vendors -->
		<link href="{!!asset('dashboard') !!}/vendors/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />

		<!--end:: Global Mandatory Vendors -->

		<!--begin:: Global Optional Vendors -->
		<link href="{!!asset('dashboard') !!}/vendors/tether/dist/css/tether.css" rel="stylesheet" type="text/css" />
		<link href="{!!asset('dashboard') !!}/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
		<link href="{!!asset('dashboard') !!}/vendors/bootstrap-datetime-picker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="{!!asset('dashboard') !!}/vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="{!!asset('dashboard') !!}/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
		<link href="{!!asset('dashboard') !!}/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" />
		<link href="{!!asset('dashboard') !!}/vendors/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" type="text/css" />
		<link href="{!!asset('dashboard') !!}/vendors/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
		<link href="{!!asset('dashboard') !!}/vendors/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />
		<link href="{!!asset('dashboard') !!}/vendors/nouislider/distribute/nouislider.css" rel="stylesheet" type="text/css" />
		<link href="{!!asset('dashboard') !!}/vendors/owl.carousel/dist/assets/owl.carousel.css" rel="stylesheet" type="text/css" />
		<link href="{!!asset('dashboard') !!}/vendors/owl.carousel/dist/assets/owl.theme.default.css" rel="stylesheet" type="text/css" />
		<link href="{!!asset('dashboard') !!}/vendors/ion-rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css" />
		<link href="{!!asset('dashboard') !!}/vendors/ion-rangeslider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet" type="text/css" />
		<link href="{!!asset('dashboard') !!}/vendors/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css" />
		<link href="{!!asset('dashboard') !!}/vendors/summernote/dist/summernote.css" rel="stylesheet" type="text/css" />
		<link href="{!!asset('dashboard') !!}/vendors/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
		<link href="{!!asset('dashboard') !!}/vendors/animate.css/animate.css" rel="stylesheet" type="text/css" />
		<link href="{!!asset('dashboard') !!}/vendors/toastr/build/toastr.css" rel="stylesheet" type="text/css" />
		<link href="{!!asset('dashboard') !!}/vendors/jstree/dist/themes/default/style.css" rel="stylesheet" type="text/css" />
		<link href="{!!asset('dashboard') !!}/vendors/morris.js/morris.css" rel="stylesheet" type="text/css" />
		<link href="{!!asset('dashboard') !!}/vendors/chartist/dist/chartist.min.css" rel="stylesheet" type="text/css" />
		<link href="{!!asset('dashboard') !!}/vendors/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet" type="text/css" />
		<link href="{!!asset('dashboard') !!}/vendors/socicon/css/socicon.css" rel="stylesheet" type="text/css" />
		<link href="{!!asset('dashboard') !!}/vendors/vendors/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css" />
		<link href="{!!asset('dashboard') !!}/vendors/vendors/flaticon/css/flaticon.css" rel="stylesheet" type="text/css" />
		<link href="{!!asset('dashboard') !!}/vendors/vendors/metronic/css/styles.css" rel="stylesheet" type="text/css" />
		<link href="{!!asset('dashboard') !!}/vendors/vendors/fontawesome5/css/all.min.css" rel="stylesheet" type="text/css" />

		<!--end:: Global Optional Vendors -->

		<!--begin::Global Theme Styles -->
{{-- 		<link href="{!!asset('dashboard') !!}/assets/demo/base/style.bundle.css" rel="stylesheet" type="text/css" />
 --}}
		<link href="{!!asset('dashboard') !!}/assets/demo/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />

		<!--end::Global Theme Styles -->
		<link rel="shortcut icon" href="{!!asset('dashboard') !!}/assets/demo/media/img/logo/favicon.ico" />
	</head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-1" id="m_login" style="background-image: url({!!asset('dashboard') !!}/assets/app/media/img//bg/bg-1.jpg);">
				<div class="m-grid__item m-grid__item--fluid m-login__wrapper">
					<div class="m-login__container">
						<div class="m-login__logo">
							<a href="#">
								<img src="{!!asset('dashboard') !!}/assets/app/media/img/logos/logo-1.png">
							</a>
						</div>
						<div class="m-login__signin">
							<div class="m-login__head">
								<h3 class="m-login__title">تسجيل دخول</h3>
                            </div>



							<form class="m-login__form m-form" method="POST" action="{{ route('admin.login') }}">
                                @csrf

                                <div class="form-group m-form__group">
                                    <input class="form-control m-input" type="text" value="{{ old('email') }}" placeholder="البريد الالكترونى" name="email" autocomplete="off">
                                    @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert" style="display:block !important">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif

								</div>
								<div class="form-group m-form__group">
									<input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password" required>
                                    @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert" style="display:block !important">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif

                                </div>
								<div class="row m-login__form-sub">
									<div class="col m--align-left m-login__form-left">
										<label class="m-checkbox  m-checkbox--light">
											<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> تذكرنى
											<span></span>
										</label>
									</div>
								</div>
								<div class="m-login__form-action">
									<button type="submit" {{-- id="m_login_signin_submit" --}} class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn m-login__btn--primary">تسجيل دخول</button>
								</div>
							</form>
                        </div>

					</div>
				</div>
			</div>
		</div>

		<!-- end:: Page -->

		<!--begin:: Global Mandatory Vendors -->
		<script src="{!!asset('dashboard') !!}/vendors/jquery/dist/jquery.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/popper.js/dist/umd/popper.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/js-cookie/src/js.cookie.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/moment/min/moment.min.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/wnumb/wNumb.js" type="text/javascript"></script>

		<!--end:: Global Mandatory Vendors -->

		<!--begin:: Global Optional Vendors -->
		<script src="{!!asset('dashboard') !!}/vendors/jquery.repeater/src/lib.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/jquery.repeater/src/jquery.input.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/jquery.repeater/src/repeater.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/jquery-form/dist/jquery.form.min.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/block-ui/jquery.blockUI.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/js/framework/components/plugins/forms/bootstrap-datepicker.init.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/js/framework/components/plugins/forms/bootstrap-timepicker.init.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/js/framework/components/plugins/forms/bootstrap-daterangepicker.init.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/bootstrap-maxlength/src/bootstrap-maxlength.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/bootstrap-switch/dist/js/bootstrap-switch.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/js/framework/components/plugins/forms/bootstrap-switch.init.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/select2/dist/js/select2.full.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/typeahead.js/dist/typeahead.bundle.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/handlebars/dist/handlebars.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/inputmask/dist/jquery.inputmask.bundle.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/inputmask/dist/inputmask/inputmask.date.extensions.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/inputmask/dist/inputmask/inputmask.numeric.extensions.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/inputmask/dist/inputmask/inputmask.phone.extensions.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/nouislider/distribute/nouislider.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/owl.carousel/dist/owl.carousel.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/autosize/dist/autosize.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/clipboard/dist/clipboard.min.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/ion-rangeslider/js/ion.rangeSlider.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/dropzone/dist/dropzone.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/summernote/dist/summernote.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/markdown/lib/markdown.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/js/framework/components/plugins/forms/bootstrap-markdown.init.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/js/framework/components/plugins/forms/jquery-validation.init.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/bootstrap-notify/bootstrap-notify.min.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/js/framework/components/plugins/base/bootstrap-notify.init.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/toastr/build/toastr.min.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/jstree/dist/jstree.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/raphael/raphael.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/morris.js/morris.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/chartist/dist/chartist.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/chart.js/dist/Chart.bundle.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/js/framework/components/plugins/charts/chart.init.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/vendors/jquery-idletimer/idle-timer.min.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/waypoints/lib/jquery.waypoints.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/counterup/jquery.counterup.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/es6-promise-polyfill/promise.min.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
		<script src="{!!asset('dashboard') !!}/vendors/js/framework/components/plugins/base/sweetalert2.init.js" type="text/javascript"></script>

		<!--end:: Global Optional Vendors -->

		<!--begin::Global Theme Bundle -->
		<script src="{!!asset('dashboard') !!}/assets/demo/base/scripts.bundle.js" type="text/javascript"></script>

		<!--end::Global Theme Bundle -->

		<!--begin::Page Scripts -->
		<script src="{!!asset('dashboard') !!}/assets/snippets/custom/pages/user/login.js" type="text/javascript"></script>

		<!--end::Page Scripts -->
	</body>

	<!-- end::Body -->
</html>






{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
 --}}
