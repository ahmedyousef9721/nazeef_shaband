<!--begin: Datatable -->
<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
    <thead>
    <tr>


        <th>#</th>
        <th>الصوره</th>
        <th>الاسم</th>
        <th>الهاتف</th>
        <th>البريد الالكترونى</th>
        <th>رقم السجل</th>
        <th>العنوان</th>
        <th>انواع الملابس</th>
        <th>المنطقه</th>
        <th>انواع الغسيل</th>
        <th>الاعدادت</th>
    </tr>
    </thead>
    <tbody>
    @foreach($laundries as $laundry)
        <tr>
            <td>{!!$loop->iteration!!}</td>
            <td><img src="{!!asset($laundry->image)!!}" height="100" width="100" /> </td>
            <td>{!!$laundry->name!!}</td>
            <td>{!!$laundry->phone!!}</td>
            <td>{!!$laundry->email!!}</td>
            <td>{!!$laundry->Commercial_number!!}</td>
            <td>{!!$laundry->address!!}</td>
            <td>
                <ul>
                @if($laundry->clothes==1)<li>    ملابس </li>@endif
                @if($laundry->carpets==1)<li>    سجاد </li>@endif
                </ul>
            </td>
            <td>{!! optional($laundry->neighborhood)->name_ar !!}</td>
            <td>
                <ul>
                    @if($laundry->clean==1)<li>    غسيل </li>@endif
                    @if($laundry->dry_clean==1)<li>    غسيل جاف </li>@endif
                    @if($laundry->clean_ironing==1)<li>    غسيل و كوى </li>@endif
                </ul>
            </td>
=            <td>
                <a href="{!!route('laundries.edit',$laundry->id)!!}" class="btn btn-primary">
                    <i class="fas fa-pen"></i>
                    تعديل</a>

                <form method="POST" action="{!!route('laundries.destroy',$laundry->id)!!}">
                    @csrf()
                     @method('delete')
                    <button type="submit" class="btn btn-danger">
                        <i class="fas fa-trash"></i>
                        حذف
                    </button>
                </form>
            </td>

        </tr>
    @endforeach
    </tbody>

</table>
