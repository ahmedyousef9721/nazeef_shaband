<div class="m-portlet__body">

    {{--'Commercial_number', 'address', 'clothes', 'carpets', 'lat', 'lng', 'neighborhood_id', 'clean', 'dry_clean', 'clean_ironing',--}}
    <div class="form-group m-form__group">
        <label for="exampleInputName">الاسم</label>

        {!! Form::text('name',null,['class'=>'form-control m-input','id'=>'exampleInputName','placeholder'=>'ادخل الاسم'])!!}
    </div>

    <div class="form-group m-form__group">
        <label for="exampleInputEmail1">البريد الالكترونى</label>

        {!! Form::email('email',null,['class'=>'form-control m-input','id'=>'exampleInputEmail1','placeholder'=>'ادخل البريد الالكترونى'])!!}
    </div>

    <div class="form-group m-form__group">
        <label for="exampleInputEmail1">الهاتف</label>

        {!! Form::text('phone',null,['class'=>'form-control m-input','id'=>'exampleInputEmail1','placeholder'=>'رقم الهاتف'])!!}
    </div>
    <div class="form-group m-form__group">
        <label for="exampleInputEmail1">رقم السجل</label>

        {!! Form::text('Commercial_number',null,['class'=>'form-control m-input','id'=>'exampleInputEmail1','placeholder'=>'رقم السجل'])!!}
    </div>
    <div class="form-group m-form__group">
        <label for="exampleInputEmail1">العنوان</label>

        {!! Form::text('address',null,['class'=>'form-control m-input','id'=>'exampleInputEmail1','placeholder'=>'العنوان'])!!}
    </div>


    <div class="form-group m-form__group">
        <label for="exampleInputPassword1">كلمه المرور</label>


        {!! Form::password('password',['class'=>'form-control m-input','id'=>'exampleInputPassword1','placeholder'=>'ادخل كلمه المرور'])!!}

    </div>

    <div class="form-group m-form__group">
        <label for="exampleInputPassword2"> تأكيد كلمه المرور </label>

        {!! Form::password('password_confirmation',['class'=>'form-control m-input','id'=>'exampleInputPassword2','placeholder'=>'ادخل تأكيد كلمه المرور'])!!}
    </div>


    <div class="m-form__group form-group row">
        <label class="col-12 col-form-label">انواع القماش</label>
        <div class="col-4">
			<span class="m-switch m-switch--info">
			<label>
                {!! Form::checkbox('clothes') !!}
                <span></span>ملابس
				</label>
				</span>
        </div>
        <div class="col-4">
			<span class="m-switch m-switch--info">
			<label>
                {!! Form::checkbox('carpets') !!}
                <span></span>سجاد
				</label>
			</span>
        </div>
   <div class="col-4">
			<span class="m-switch m-switch--info">
			<label>
                {!! Form::checkbox('mattresses') !!}
                <span></span>مفارش
				</label>
			</span>
        </div>

    </div>
    <div class="m-form__group form-group row">
        <label class="col-12 col-form-label">انواع الغسيل</label>
        <div class="col-4">
			<span class="m-switch m-switch--info">
			<label>
                {!! Form::checkbox('clean') !!}
                <span></span>غسيل
				</label>
				</span>
        </div>
        <div class="col-4">
			<span class="m-switch m-switch--info">
			<label>
                {!! Form::checkbox('dry_clean') !!}
                <span></span>غسيل جاف
				</label>
				</span>
        </div>
        <div class="col-4">
			<span class="m-switch m-switch--info">
			<label>
                {!! Form::checkbox('clean_ironing') !!}
                <span> </span>غسيل  ومكوي
				</label>
				</span>
        </div>

    </div>

    <div class="form-group m-form__group">
        @if(isset($user))
            <img src={!!asset($user->image)!!} width="100" height="100">
        @endif
        <label for="imageInput"> الصوره </label>
        <input type="file" class="form-control m-input" name="image" id="imageInput">
    </div>
</div>

{!! Form::hidden('lat',null)!!}
{!! Form::hidden('lng',null)!!}
<div id='map' style="width: 100%; height: 500px;"></div>
</div>
@if(isset($laundry))
    <script>
        var lat={!! $laundry->lat !!}
        var lng={!! $laundry->lat !!}


    </script>

@endif
