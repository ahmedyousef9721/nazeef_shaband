<!--begin: Datatable -->
<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
    <thead>
    <tr>
        <th>#</th>
        <th>الصوره</th>
        <th>الاسم</th>
        <th>الهاتف</th>
        <th>البريد الالكترونى</th>
        <th>النوع</th>
        <th>الراتب</th>
        <th>النسبه</th>
        <th>الشركه</th>
        <th>الاعدادت</th>
    </tr>
    </thead>
    <tbody>
    @foreach($deliveries as $delivery)
        <tr>
            <td>{!!$loop->iteration!!}</td>
            <td><img src="{!!asset($delivery->image)!!}" height="100" width="100" /> </td>
            <td>{!!$delivery->name!!}</td>
            <td>{!!$delivery->phone!!}</td>
            <td>{!!$delivery->email!!}</td>
            <td>{!!$delivery->type!!}</td>
            <td>{!!$delivery->salary!!}</td>
            <td>{!!$delivery->percent!!}</td>
            <td>{!!$delivery->user->name!!}</td>
            <td>
                <a href="{!!route('deliveries.edit',$delivery->id)!!}" class="btn btn-primary"> <i class="fas fa-pen"></i>  تعديل</a>

                <form method="POST" action="{!!route('deliveries.destroy',$delivery->id)!!}">
                    @csrf()
                     @method('delete')
                    <button type="submit" class="btn btn-danger">
                        <i class="fas fa-trash"></i>
                        حذف
                    </button>
                </form>
            </td>

        </tr>
    @endforeach
    </tbody>

</table>
