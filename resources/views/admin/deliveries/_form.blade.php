<div class="m-portlet__body">

    <div class="form-group m-form__group">
        <label for="exampleInputName">الاسم</label>

        {!! Form::text('name',null,['class'=>'form-control m-input','id'=>'exampleInputName','placeholder'=>'ادخل الاسم'])!!}
    </div>

    <div class="form-group m-form__group">
        <label for="exampleInputEmail1">البريد الالكترونى</label>

        {!! Form::email('email',null,['class'=>'form-control m-input','id'=>'exampleInputEmail1','placeholder'=>'ادخل البريد الالكترونى'])!!}
    </div>

    <div class="form-group m-form__group">
        <label for="exampleInputEmail1">الهاتف</label>

        {!! Form::text('phone',null,['class'=>'form-control m-input','id'=>'exampleInputEmail1','placeholder'=>'رقم الهاتف'])!!}
    </div>


    <div class="form-group m-form__group">
        <label for="exampleInputPassword1">كلمه المرور</label>


        {!! Form::password('password',['class'=>'form-control m-input','id'=>'exampleInputPassword1','placeholder'=>'ادخل كلمه المرور'])!!}

    </div>

    <div class="form-group m-form__group">
        <label for="exampleInputPassword2"> تأكيد كلمه المرور </label>

        {!! Form::password('password_confirmation',['class'=>'form-control m-input','id'=>'exampleInputPassword2','placeholder'=>'ادخل تأكيد كلمه المرور'])!!}
    </div>




    <div class="form-group m-form__group">
        <label for="exampleInputPassword2"> النوع </label>
        {!! Form::select('type',['independent'=>'مستقل','company'=>'تابع لشركه','website'=>'تابع للموقع'],null,['class'=>'form-control m-input select2','id'=>'exampleInputPassword2','placeholder'=>'النوع' ,'onChange'=>'toggleSalariesInputs(this.value)'])!!}
    </div>

    <div class="form-group m-form__group company-group">
        <label for="exampleInputPassword2"> الشركه </label>
        {!! Form::select('user_id',$users->pluck('name','id'),null,['class'=>'form-control m-input select2','id'=>'exampleInputPassword2','placeholder'=>'الشركه'])!!}
    </div>

    <div class="form-group m-form__group">
        <label for="exampleInputPassword2"> حجم السياره </label>

        {!! Form::select('car_type',['small'=>'صغير','big'=>'كبير'],null,['class'=>'form-control m-input select2','id'=>'exampleInputPassword2','placeholder'=>'حجم السياره'])!!}
    </div>
    <div class="form-group m-form__group salary-group">
        <label for="exampleInputPassword2"> الراتب الشهرى </label>
        {!! Form::number('salary',null,['class'=>'form-control m-input','id'=>'exampleInputPassword2','placeholder'=>'الراتب الشهرى'])!!}
    </div>
   <div class="form-group m-form__group percent-group" >
        <label for="exampleInputPassword2"> نسبه الحساب </label>
        {!! Form::number('percent',null,['class'=>'form-control m-input','id'=>'exampleInputPassword2','placeholder'=>'نسبه الحساب '])!!}
    </div>

    <div class="m-form__group form-group row">
        <label class="col-12 col-form-label">الحساب مفعل</label>
        <div class="col-12">
			<span class="m-switch m-switch--info">
			<label>
                {!! Form::checkbox('active') !!}
                <span></span>
				</label>
				</span>
        </div>

    </div>

    <div class="form-group m-form__group">
        @if(isset($user))
        <img src={!!asset($user->image)!!} width="100" height="100">
        @endif
        <label for="imageInput"> الصوره </label>
        <input type="file" class="form-control m-input" name="image" id="imageInput">
    </div>
</div>

