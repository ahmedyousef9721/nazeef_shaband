<!--begin: Datatable -->
<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
    <thead>
    <tr>
        <th>التسلسل</th>
        <th>الاسم بالعربيه</th>
        <th>الاسم بالانجليزيه</th>

        <th>الاعدادت</th>
    </tr>
    </thead>
    <tbody>
    @foreach($areas as $area)
        <tr>
            <td>{!!$loop->iteration!!}</td>
           
            <td>{!!$area->name_ar!!}</td>
            <td>{!!$area->name_en!!}</td>
            
            <td>
                <a href="{!!route('areas.edit',$area->id)!!}" class="btn btn-primary">
                    <i class="fas fa-pen"></i>
                    تعديل</a>

                <form method="POST" action="{!!route('areas.destroy',$area->id)!!}">
                    @csrf()
                     @method('delete')
                    <button type="submit" class="btn btn-danger">
                        <i class="fas fa-trash"></i>
                        حذف
                    </button>
                </form>
            </td>
        </tr>


    @endforeach

    </tbody>

</table>
