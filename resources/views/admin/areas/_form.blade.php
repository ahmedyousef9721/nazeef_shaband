<div class="m-portlet__body">

    <div class="form-group m-form__group">
        <label for="exampleInputName">الاسم بالعربيه</label>

        {!! Form::text('name_ar',null,['class'=>'form-control m-input','id'=>'exampleInputName','placeholder'=>'ادخل الاسم بالعربيه'])!!}
    </div>

    <div class="form-group m-form__group">
        <label for="exampleInputEmail1">ادخل الاسم بالانجليزيه</label>

        {!! Form::text('name_en',null,['class'=>'form-control m-input','id'=>'exampleInputEmail1','placeholder'=>'ادخل الاسم بالانجليزيه'])!!}
    </div>

    <div class="form-group m-form__group">
        <label for="exampleInputEl1">ادخل نقاط الحدود شرط ان تكون بالترتيب كما هى ع الخريطه</label>

        {!! Form::select('borders[]',[],null,['class'=>'form-control m-input taggable-select2','id'=>'exampleInputE1','multiple'])!!}
    </div>


</div>
