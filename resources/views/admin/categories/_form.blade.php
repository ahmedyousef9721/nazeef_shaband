<div class="m-portlet__body">
    {{--'name_ar', 'name_en', 'image', 'type', 'clean_price', 'dry_clean_price', 'clean_ironing_price', 'fast_clean_price', 'fast_dry_clean_price', 'fast_clean_ironing_price'--}}
    <div class="form-group m-form__group">
        <label for="exampleInputName">الاسم بالعربيه</label>

        {!! Form::text('name_ar',null,['class'=>'form-control m-input','id'=>'exampleInputName','placeholder'=>'ادخل الاسم بالعربيه'])!!}
    </div>

    <div class="form-group m-form__group">
        <label for="exampleInputEmail1">ادخل الاسم بالانجليزيه</label>
        {!! Form::text('name_en',null,['class'=>'form-control m-input','id'=>'exampleInputEmail1','placeholder'=>'ادخل الاسم بالانجليزيه'])!!}
    </div>

    <div class="form-group m-form__group">
        <label for="exampleInputEmail1">النوع</label>
        {!! Form::select('type',['clothes'=>'ملابس','carpets'=>'سجاد','mattresses'=>'مفارش'],null,['class'=>'form-control m-input select2','id'=>'exampleInputEmail1','placeholder'=>'النوع','onchange'=>'toggleFastOrder(this.value)'])!!}
    </div>


    <div class="form-group m-form__group">
        <label for="exampleInputEmail1">سعر الغسيل</label>
        {!! Form::number('clean_price',null,['class'=>'form-control m-input','id'=>'exampleInputEmail1','placeholder'=>'سعر الغسيل'])!!}
    </div>

    <div class="form-group m-form__group">
        <label for="exampleInputEmail1">سعر الغسيل الجاف</label>
        {!! Form::number('dry_clean_price',null,['class'=>'form-control m-input','id'=>'exampleInputEmail1','placeholder'=>'سعر الغسيل الجاف'])!!}
    </div>

    <div class="form-group m-form__group">
        <label for="exampleInputEmail1">سعر الغسيل والكى</label>
        {!! Form::number('clean_ironing_price',null,['class'=>'form-control m-input','id'=>'exampleInputEmail1','placeholder'=>'سعر الغسيل والكى'])!!}
    </div>

    <div class="fastOrder @if(isset($category)&&$category->type=='clothes') d-block @else d-none @endif"
    >
        <div class="form-group m-form__group">
            <label for="exampleInputEmail1">سعر الغسيل سريع</label>
            {!! Form::number('fast_clean_price',null,['class'=>'form-control m-input','id'=>'exampleInputEmail1','placeholder'=>'سعر الغسيل'])!!}
        </div>

        <div class="form-group m-form__group">
            <label for="exampleInputEmail1">سعر الغسيل الجاف سريع</label>
            {!! Form::number('fast_dry_clean_price',null,['class'=>'form-control m-input','id'=>'exampleInputEmail1','placeholder'=>'سعر الغسيل الجاف'])!!}
        </div>

        <div class="form-group m-form__group">
            <label for="exampleInputEmail1">سعر الغسيل والكى سريع</label>
            {!! Form::number('fast_clean_ironing_price',null,['class'=>'form-control m-input','id'=>'exampleInputEmail1','placeholder'=>'سعر الغسيل والكى'])!!}
        </div>

    </div>

    <div class="form-group m-form__group">
        <label for="exampleInputEmail1">الصوره</label>
        {!! Form::file('image',['class'=>'form-control '])!!}
    </div>

</div>
@push('scripts')

@endpush
