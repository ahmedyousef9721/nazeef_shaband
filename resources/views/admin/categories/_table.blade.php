<!--begin: Datatable -->
<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
    <thead>
    <tr>
{{--
        'name_ar', 'name_en', 'image', 'type', 'clean_price', 'dry_clean_price', 'clean_ironing_price', 'fast_clean_price', 'fast_dry_clean_price', 'fast_clean_ironing_price'--}}
        <th>التسلسل</th>
        <th>الصوره</th>
        <th>الاسم بالعربيه</th>
        <th>الاسم بالانجليزيه</th>
        <th>النوع</th>
        <th>سعر الغسيل</th>
        <th>سعر الغسيل والمكوى</th>
        <th>سعر الغسيل الجاف</th>
        <th>سعر الغسيل السريع</th>
        <th>سعر الغسيل والمكوى السريع</th>
        <th>سعر الغسيل الجاف السريع</th>

        <th>الاعدادت</th>
    </tr>
    </thead>
    <tbody>
    @foreach($categories as $category)
        <tr>
            <td>{!!$loop->iteration!!}</td>
           
            <td> <img src="{!! asset($category->image) !!}" width="100" height="100" alt="{!! $category->name !!}"></td>
            <td>{!!$category->name_ar!!}</td>
            <td>{!!$category->name_en!!}</td>
            <td>{!!$category->type!!}</td>
            <td>{!!$category->clean_price!!}</td>
            <td>{!!$category->dry_clean_price!!}</td>
            <td>{!!$category->clean_ironing_price!!}</td>
            <td>{!!$category->fast_clean_price!!}</td>
            <td>{!!$category->fast_dry_clean_price!!}</td>
            <td>{!!$category->fast_clean_ironing_price!!}</td>

            <td>
                <a href="{!!route('categories.edit',$category->id)!!}" class="btn btn-primary">
                    <i class="fas fa-pen"></i>
                    تعديل
                </a>

                <form method="POST" action="{!!route('categories.destroy',$category->id)!!}">
                    @csrf()
                     @method('delete')
                    <button type="submit" class="btn btn-danger">
                        <i class="fas fa-trash"></i>
                        حذف
                    </button>
                </form>
            </td>
        </tr>


    @endforeach

    </tbody>

</table>
