<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBordersTable extends Migration {

	public function up()
	{
		Schema::create('borders', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('model_id');
			$table->string('model_type');
			$table->string('lat');
			$table->string('lng');
		});
	}

	public function down()
	{
		Schema::drop('borders');
	}
}