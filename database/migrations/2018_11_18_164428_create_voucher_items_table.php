<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVoucherItemsTable extends Migration {

	public function up()
	{
		Schema::create('voucher_items', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('voucher_id');
			$table->integer('category_id')->unsigned();
			$table->decimal('amount');
			$table->decimal('total');
			$table->string('price');
		});
	}

	public function down()
	{
		Schema::drop('voucher_items');
	}
}