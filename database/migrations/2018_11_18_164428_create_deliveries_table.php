<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDeliveriesTable extends Migration {

	public function up()
	{
		Schema::create('deliveries', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('email');
			$table->string('phone');
			$table->string('password');
			$table->string('lat');
			$table->string('lng');
			$table->string('image');
			$table->enum('car_type', array('small', 'big'));
			$table->string('type')->default('independent,company,website');
			$table->integer('user_id')->nullable();
			$table->string('salary');
			$table->string('percent')->nullable();
			$table->string('verify_mobile');
			$table->timestamp('mobile_verified')->nullable();
			$table->boolean('active');
			$table->string('api_token');
			$table->unsignedInteger('neighborhood_id')->nullable();
            $table->text('personal_id')->nullable();
            $table->text('drive_license')->nullable();
            $table->text('car_license')->nullable();
            $table->text('criminal_testimony_pic')->nullable();
            $table->text('car_front')->nullable();
            $table->text('car_back')->nullable();
			$table->timestamps();
            $table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('deliveries');
	}
}
