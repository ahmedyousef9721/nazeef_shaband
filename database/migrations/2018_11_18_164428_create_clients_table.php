<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientsTable extends Migration {

	public function up()
	{
		Schema::create('clients', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('email');
			$table->string('password');
			$table->string('image');
			$table->string('phone');
			$table->string('verify_mobile');
			$table->timestamp('mobile_verified')->nullable();
			$table->decimal('balance')->default('0');
			$table->boolean('active');
			$table->timestamps();
			$table->softDeletes();
			$table->string('fb_token')->nullable();
			$table->string('twitter_token')->nullable();
			$table->string('google_token')->nullable();
			$table->boolean('is_boot');
			$table->string('api_token');
		});
	}

	public function down()
	{
		Schema::drop('clients');
	}
}