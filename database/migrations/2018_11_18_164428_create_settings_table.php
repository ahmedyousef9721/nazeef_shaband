<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSettingsTable extends Migration {

	public function up()
	{
		Schema::create('settings', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name', 255)->nullable();
			$table->string('value_ar', 255)->nullable();
			$table->string('value_en', 255)->nullable();
			$table->string('slug', 255)->nullable();
			$table->tinyInteger('type');
		});
	}

	public function down()
	{
		Schema::drop('settings');
	}
}