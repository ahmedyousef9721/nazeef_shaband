<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRefusedVouchersTable extends Migration {

	public function up()
	{
		Schema::create('refused_vouchers', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('voucher_id')->unsigned();
			$table->integer('model_id')->unsigned();
			$table->string('model_type');
			$table->string('note')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('refused_vouchers');
	}
}