<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLaundriesTable extends Migration {

	public function up()
	{
		Schema::create('laundries', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('email');
			$table->string('password');
			$table->string('image');
			$table->string('phone')->nullable();
			$table->string('Commercial_number')->nullable();
			$table->string('address')->nullable();
			$table->boolean('clothes');
			$table->boolean('carpets')->nullable();
			$table->boolean('mattresses')->nullable();
			$table->string('lat');
			$table->string('lng');
			$table->integer('neighborhood_id')->nullable();
			$table->boolean('clean');
			$table->boolean('dry_clean');
			$table->boolean('clean_ironing');
			$table->string('api_token');
			$table->string('verify_mobile');
			$table->timestamp('mobile_verified');
			$table->text('Commercial_cert');
			$table->text('laundry_personal_id');
			$table->text('shop_personal_id');
			$table->timestamps();
            $table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('laundries');
	}
}
