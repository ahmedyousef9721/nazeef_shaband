<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoriesTable extends Migration {

	public function up()
	{
		Schema::create('categories', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('name_ar');
			$table->string('name_en');
			$table->string('image');
			$table->enum('type',['clothes','carpets','mattresses'])->default('clothes');
			$table->decimal('clean_price');
			$table->decimal('dry_clean_price');
			$table->decimal('clean_ironing_price');
			$table->decimal('fast_clean_price')->nullable();
			$table->decimal('fast_dry_clean_price')->nullable();
			$table->decimal('fast_clean_ironing_price')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('categories');
	}
}
