<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNeighborhoodsTable extends Migration {

	public function up()
	{
		Schema::create('neighborhoods', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('name_ar');
			$table->string('name_en');
			$table->string('city_id');
		});
	}

	public function down()
	{
		Schema::drop('neighborhoods');
	}
}