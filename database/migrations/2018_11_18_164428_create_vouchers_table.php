<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVouchersTable extends Migration {

	public function up()
	{
		Schema::create('vouchers', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('client_id');
			$table->string('laundry_id');
			$table->integer('delivery_from_id');
			$table->string('delivery_to_id')->nullable();
			$table->boolean('is_fast')->nullable();
			$table->enum('status', array('pending', 'to_laundry', 'at_laundry', 'form_laundry', 'finished', 'canceled'));
			$table->decimal('delivery_from_cost');
			$table->string('delivery_to_cost');
			$table->string('total');
			$table->timestamp('delivery_from_received_at')->nullable();
			$table->string('delivery_to_received_at')->nullable();
			$table->timestamp('laundry_reciver_at')->nullable();
			$table->timestamp('finished_at')->nullable();
			$table->timestamp('client_received_at')->nullable();
			$table->string('code');
		});
	}

	public function down()
	{
		Schema::drop('vouchers');
	}
}