<?php

Route::group(['namespace' => 'Admin'], function () {
    Route::group(['middleware' => ['auth']], function () {
        Route::get('/', 'HomeController@index')->name('admin.dashboard');
        Route::resource('users', 'userController');
        Route::resource('clients', 'clientController');
        Route::resource('deliveries', 'deliveryController');
        Route::resource('laundries', 'laundryController');
        Route::resource('cities', 'cityController');
        Route::resource('areas', 'areaController');
        Route::resource('neighborhoods', 'neighborhoodController');
        Route::resource('categories', 'categoryController');
        Route::resource('tickets', 'ticketController');
        Route::resource('addresses', 'addressController');
    });
    // // Login
    // Route::get('login', 'Auth\LoginController@showLoginForm')->name('admin.login');
    // Route::post('login', 'Auth\LoginController@login');
    // Route::get('logout', 'Auth\LoginController@logout')->name('admin.logout');

    // // Register
    // Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('admin.register');
    // Route::post('register', 'Auth\RegisterController@register');

    // // Passwords
    // Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    // Route::post('password/reset', 'Auth\ResetPasswordController@reset');
    // Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    // Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('admin.password.reset');

    // Verify
    // Route::get('email/resend', 'Auth\VerificationController@resend')->name('admin.verification.resend');
    // Route::get('email/verify', 'Auth\VerificationController@show')->name('admin.verification.notice');
    // Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('admin.verification.verify');
});
