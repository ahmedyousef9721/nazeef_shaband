<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/




Route::post('register', 'authController@register');
Route::post('login', 'authController@login');
Route::Post('resend_verify', 'authController@resend_verify');
Route::post('verify', 'authController@verifyProfile');

Route::group(['middleware'=>'auth:laundries_api'],function () {
    Route::post('profile', 'authController@profile');
});
